<?php

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Include the Pantheon-specific settings file.
 *
 * n.b. The settings.pantheon.php file makes some changes
 *      that affect all environments that this site
 *      exists in.  Always include this file, even in
 *      a local development environment, to ensure that
 *      the site settings remain consistent.
 */
include __DIR__ . "/settings.pantheon.php";

/**
 * Skipping permissions hardening will make scaffolding
 * work better, but will also raise a warning when you
 * install Drupal.
 *
 * https://www.drupal.org/project/drupal/issues/3091285
 */
// $settings['skip_permissions_hardening'] = TRUE;

/**
 * If there is a local settings file, then include it
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}

/**
 * Update search api server config based on current environment.
 */
if (!empty($_ENV['PANTHEON_ENVIRONMENT'])) {
  switch($_ENV['PANTHEON_ENVIRONMENT']) {
    case 'live':
      // keys for production env
      $secrets_filename = 'live.secrets.json';
      break;
    case 'test':
      // keys for staging env
      $secrets_filename = 'test.secrets.json';
      break;
    default:
      // keys for dev and multidev envs
      $secrets_filename = 'dev.secrets.json';
      break;
  }
  if (file_exists('/files/private/' . $secrets_filename)) {
    $secrets_json_text = file_get_contents('/files/private/' . $secrets_filename);
    if (!empty($secrets_data = json_decode($secrets_json_text, TRUE))) {
      $config['search_api.server.dxp_solr']['backend_config']['connector_config'] = [
        'scheme' => 'https',
        'host' => $secrets_data['solr_host_' . $_ENV['PANTHEON_ENVIRONMENT']],
        'port' => '8983',
        'path' => '/',
        'core' => $secrets_data['solr_core_' . $_ENV['PANTHEON_ENVIRONMENT']],
        'timeout' => '5',
        'index_timeout' => '5',
        'optimize_timeout' => '10',
        'finalize_timeout' => '30',
        'skip_schema_check' => 'false',
        'solr_version' => '',
        'http_method' => 'AUTO',
        'commit_within' => '1000',
        'jmx' => 'false',
        'jts' => 'false',
        'solr_install_dir' => '',
        'username' => $secrets_data['solr_user_' . $_ENV['PANTHEON_ENVIRONMENT']],
        'password' => $secrets_data['solr_pass_' . $_ENV['PANTHEON_ENVIRONMENT']],
      ];
    }
  }
}
