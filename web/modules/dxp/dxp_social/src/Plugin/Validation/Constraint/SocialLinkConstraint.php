<?php

namespace Drupal\dxp_social\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Link validation constraint for a social component.
 *
 * @Constraint(
 *   id = "SocialLink",
 *   label = @Translation("A social link must point to an approved social media platform.", context = "Validation"),
 * )
 */
class SocialLinkConstraint extends Constraint {

  /**
   * The message shown when an item's URI is not an approved social platform.
   *
   * @var string
   */
  public string $message = '@uri is not an approved social media platform.';

}
