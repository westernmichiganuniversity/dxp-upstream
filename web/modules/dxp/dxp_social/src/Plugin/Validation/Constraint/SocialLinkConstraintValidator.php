<?php

namespace Drupal\dxp_social\Plugin\Validation\Constraint;

use Drupal\link\LinkItemInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Constraint validator for social links.
 */
class SocialLinkConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    /** @var \Drupal\Core\Field\FieldItemListInterface $value */
    /** @var \Drupal\dxp_social\Plugin\Validation\Constraint\SocialLinkConstraint $constraint */
    if (!isset($value)) {
      return;
    }

    // Check each field value against the list of allowed social links.
    foreach ($value as $delta => $item) {
      if (!$item->isEmpty() && $item instanceof LinkItemInterface) {
        $url = $item->getUrl();
        $uri = $url->getUri();
        $scheme = parse_url($uri, PHP_URL_SCHEME);
        // Only allow internal links (so we can link to contact forms), links to
        // tel: and mailto:, and links to approved social platforms.
        if ($url->isExternal()
            && $scheme !== 'tel'
            && $scheme !== 'mailto'
            && !$this->isSocial($uri)) {
          $this->context
            ->buildViolation($constraint->message)
            ->setParameter('@uri', parse_url($uri, PHP_URL_HOST))
            ->atPath($delta . '.uri')
            ->addViolation();
        }
      }
    }
  }

  /**
   * Tests whether a URI points to an allowed social platform.
   *
   * @param string $uri
   *   The URI.
   *
   * @return bool
   *   TRUE if the URI points to an approved social platform.
   */
  private function isSocial(string $uri): bool {
    // Approved social domains, plus wmich.edu (for links to contact forms).
    $approved = [
      '/^(.*\.)?facebook\.com$/',
      '/^(.*\.)?instagram\.com$/',
      '/^(.*\.)?linkedin\.com$/',
      '/^(.*\.)?snapchat\.com$/',
      '/^(.*\.)?soundcloud\.com$/',
      '/^(.*\.)?tiktok\.com$/',
      '/^(.*\.)?twitter\.com$/',
      '/^(.*\.)?weibo\.com$/',
      '/^(.*\.)?youtube\.com$/',
      '/^(.*\.)?wmich\.edu$/',
    ];
    $host = parse_url($uri, PHP_URL_HOST);
    foreach ($approved as $pattern) {
      if (preg_match($pattern, $host)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
