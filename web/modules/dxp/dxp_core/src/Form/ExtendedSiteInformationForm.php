<?php

namespace Drupal\dxp_core\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Form\SiteInformationForm;

/**
 * Used to add custom fields to the Site Information config form.
 */
class ExtendedSiteInformationForm extends SiteInformationForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $site_config = $this->config('system.site');
    $form = parent::buildForm($form, $form_state);
    $form['site_information']['short_site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Short site name'),
      '#default_value' => $site_config->get('short_site_name') ?: '',
      '#description' => $this->t("A shorter site name for use in breadcrumbs"),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('system.site')
      ->set('short_site_name', $form_state->getValue('short_site_name'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
