<?php

namespace Drupal\dxp_core\Plugin\Field\FieldType;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Represents a computed field that tracks an entity's heading level.
 */
class DxpHeadingLevelFieldItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $entity = $this->getEntity();
    $value['value'] = $this->getHeadingLevel($entity);
    $this->list[0] = $this->createItem(0, $value);
  }

  /**
   * Finds an entity's heading level.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   A fieldable entity.
   *
   * @return int
   *   A heading level value from 1 to 6.
   */
  protected function getHeadingLevel(FieldableEntityInterface $entity) {
    $level = 1;
    if (!$entity->isNew() && $entity instanceof ParagraphInterface) {
      // A paragraph inherits its parent entity's heading level.
      $parent = $entity->getParentEntity();
      if ($parent->hasField('dxp_heading_level')) {
        $parent_level = $parent->get('dxp_heading_level')->getValue();
        $level = $parent_level[0]['value'];
      }
      // If the paragraph has a heading, increment the heading level (max 6).
      if ($level < 6 && $entity->hasField('field_heading') && $entity->get('field_heading')->getValue()) {
        // Only increment if the use heading element field is not present or
        // is checked.
        if (!($entity->hasField('field_use_heading_element') && $entity->get('field_use_heading_element')->getValue()[0]['value'] == 0)) {
          $level++;
        }
      }
    }
    return $level;
  }

}
