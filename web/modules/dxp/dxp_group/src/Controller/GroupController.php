<?php

namespace Drupal\dxp_group\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides Group route controller.
 */
class GroupController {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param string $menu
   *   Menu parameter from route.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function accessManageMenu(AccountInterface $account, string $menu) {
    if (in_array('administrator', $account->getRoles(TRUE))) {
      return AccessResult::allowed();
    }
    if (!empty($menu)) {
      $is_member = FALSE;
      $membership_service = \Drupal::service('group.membership_loader');
      $groups = $membership_service->loadByUser($account);
      foreach ($groups as $grp) {
        $group = $grp->getGroup();
        if ($group->get('field_menu')->getValue()[0]['target_id'] == $menu) {
          $is_member = TRUE;
          break;
        }
      }
      return AccessResult::allowedIf($is_member);
    }
    return AccessResult::forbidden();
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function accessAddMenu(AccountInterface $account) {
    return AccessResult::allowedIf(in_array('administrator', $account->getRoles(TRUE)));
  }

}
