<?php

namespace Drupal\dxp_group\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.group.canonical')) {
      $route->addOptions(['_admin_route' => TRUE]);
    }
    if ($route = $collection->get('view.group_members.page_1')) {
      $route->addOptions(['_admin_route' => TRUE]);
    }
    if ($route = $collection->get('view.group_nodes.page_1')) {
      $route->addOptions(['_admin_route' => TRUE]);
    }
    if ($route = $collection->get('entity.group.revision')) {
      $route->addOptions(['_admin_route' => TRUE]);
    }
    if ($route = $collection->get('entity.group.version_history')) {
      $route->addOptions(['_admin_route' => TRUE]);
    }

  }

}
