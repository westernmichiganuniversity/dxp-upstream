import DrupalAttribute from "drupal-attribute";
import AlertTwig from "../../../../themes/dxp/dxp_ui/components/02-molecules/alert/alert.twig";

let now = new Date();
now = now.toISOString();
fetch(
  `/qYgzLWDpk3u5/jsonapi/site_alert/site_alert?filter[active]=1&filter[start][condition][path]=scheduling.value&filter[start][condition][operator]=<&filter[start][condition][value]=${now}&filter[end][condition][path]=scheduling.end_value&filter[end][condition][operator]=>&filter[end][condition][value]=${now}`,
  {
    method: "GET",
    headers: { Accept: "application/vnd.api+json" },
  }
)
  .then((response) => response.json())
  .then((json) => {
    let dxpAlert = "";
    json.data.forEach((siteAlert) => {
      dxpAlert += AlertTwig({
        attributes: new DrupalAttribute(),
        labelAttributes: new DrupalAttribute(),
        label: siteAlert.attributes.label,
        textAttributes: new DrupalAttribute(),
        content: siteAlert.attributes.message.value,
      });
    });
    document.getElementById("dxp_alert").innerHTML = dxpAlert;
  });
