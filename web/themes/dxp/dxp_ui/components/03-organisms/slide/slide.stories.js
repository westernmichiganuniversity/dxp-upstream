import DrupalAttribute from "drupal-attribute";
import SlideTwig from "./slide.twig";
import { Image } from "../../01-atoms/image/image.stories";

export default {
  title: "Organisms/Slide",
  argTypes: {
    mediaAspect: {
      control: { type: "select" },
      options: ["2:3", "1:1", "4:3", "3:2", "16:9", "auto"],
    },
    heading: { control: "text" },
    headingLevel: {
      control: { type: "select" },
      options: [1, 2, 3, 4, 5, 6],
    },
    text: { control: "text" },
    links: { control: "text" },
    overlayCaption: { control: "boolean" },
    halign: {
      control: { type: "select" },
      options: ["left", "center", "right"],
    },
    valign: {
      control: { type: "select" },
      options: ["top", "middle", "bottom"],
    },
    width: {
      control: { type: "select" },
      options: ["full", "3/4", "2/3", "1/2", "1/3", "1/4"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const captionAttributes = new DrupalAttribute();
  const calloutAttributes = new DrupalAttribute();
  const headingAttributes = new DrupalAttribute();
  const textAttributes = new DrupalAttribute();
  return SlideTwig({
    attributes,
    captionAttributes,
    calloutAttributes,
    headingAttributes,
    textAttributes,
    ...args,
  });
};

export const Slide = Template.bind({});
Slide.args = {
  media: Image({ ...Image.args }),
  mediaAspect: "16:9",
  heading: "Find a mentor, make a friend",
  headingLevel: 2,
  text: "Instructors share wisdom and experience not found in textbooks.",
  halign: "right",
  valign: "bottom",
  width: "full",
  overlayCaption: true,
};
