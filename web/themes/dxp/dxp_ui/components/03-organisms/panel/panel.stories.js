import DrupalAttribute from "drupal-attribute";
import PanelTwig from "./panel.twig";
import { Text } from "../../01-atoms/text/text.stories";
import { Callout } from "../../02-molecules/callout/callout.stories";

export default {
  title: "Organisms/Panel",
  argTypes: {
    halign: {
      control: { type: "select" },
      options: ["left", "center", "right"],
    },
    bgcolor: {
      control: { type: "select" },
      options: ["cool_gray", "gray", "warm_gray"],
    },
    bgpattern: {
      control: { type: "select" },
      options: ["circles", "diamonds"],
    },
    vmargin: {
      control: { type: "select" },
      options: ["normal", "large"],
    },
    variant: {
      control: { type: "select" },
      options: ["background", "border"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const content = args.content.join("");
  return PanelTwig({ ...args, attributes, content });
};

const lightText = Text({
  ...Text.args,
  withMargin: true,
  content:
    "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id diam mi. Sed quis velit non elit euismod pellentesque. Aliquam et felis dapibus risus dictum dapibus in sed arcu.</p>",
  colorMode: "light",
});
const lightCallout = Callout({
  ...Callout.args,
  linkDisplay: "primary_secondary",
  colorMode: "light",
});
const darkText = Text({
  ...Text.args,
  withMargin: true,
  content:
    "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id diam mi. Sed quis velit non elit euismod pellentesque. Aliquam et felis dapibus risus dictum dapibus in sed arcu.</p>",
  colorMode: "dark",
});
const darkCallout = Callout({
  ...Callout.args,
  linkDisplay: "primary_secondary",
  colorMode: "dark",
});

export const Panel = Template.bind({});
Panel.args = {
  halign: "left",
  bgcolor: "gray",
  bgpattern: "circles",
  variant: "background",
  content: [lightText, lightCallout],
  colorMode: "light",
};

export const DarkPanel = Template.bind({});
DarkPanel.args = {
  halign: "left",
  bgcolor: "warm_gray",
  bgpattern: "circles",
  variant: "background",
  content: [darkText, darkCallout],
  colorMode: "dark",
};
