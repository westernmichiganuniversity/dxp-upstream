import DrupalAttribute from "drupal-attribute";
import FooterTwig from "./footer.twig";

export default {
  title: "Organisms/Footer",
  argTypes: {
    items: { control: { type: "object" } },
    links: { control: { type: "object" } },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const menuAttributes = new DrupalAttribute();
  const socialAttributes = new DrupalAttribute();
  const textAttributes = new DrupalAttribute();
  const linkAttributes = new DrupalAttribute();
  const links = [];
  args.links.forEach((item) => {
    links.push({
      attributes: new DrupalAttribute(),
      iconAttributes: new DrupalAttribute(),
      label: item.label,
      href: item.href,
    });
  });
  return FooterTwig({
    ...args,
    attributes,
    menuAttributes,
    socialAttributes,
    links,
    textAttributes,
    linkAttributes,
  });
};

export const Footer = Template.bind({});
Footer.args = {
  items: [
    {
      attributes: new DrupalAttribute(),
      label: "Audiences",
      href: "",
      inActiveTrail: true,
      type: "nolink",
      children: {
        id: "submenu-audiences",
        attributes: new DrupalAttribute().setAttribute(
          "id",
          "submenu-audiences"
        ),
        items: [
          {
            attributes: new DrupalAttribute(),
            label: "Alumni and Donors",
            href: "https://wmualumni.org/",
            type: "link",
          },
          {
            attributes: new DrupalAttribute(),
            label: "Businesses",
            href: "https://wmich.edu/businessconnection",
            inActiveTrail: true,
            type: "link",
          },
          {
            attributes: new DrupalAttribute(),
            label: "Veterans",
            href: "https://wmich.edu/military",
            type: "link",
          },
        ],
      },
    },
    {
      attributes: new DrupalAttribute(),
      label: "Locations",
      href: "",
      type: "nolink",
      children: {
        id: "submenu-locations",
        attributes: new DrupalAttribute().setAttribute(
          "id",
          "submenu-locations"
        ),
        items: [
          {
            attributes: new DrupalAttribute(),
            label: "Kalamazoo",
            href: "https://wmich.edu/about/kalamazoo",
            type: "link",
          },
          {
            attributes: new DrupalAttribute(),
            label: "Regional Locations",
            href: "https://wmich.edu/extended",
            type: "link",
          },
          {
            attributes: new DrupalAttribute(),
            label: "Online Education",
            href: "https://wmich.edu/online",
            type: "link",
          },
          {
            attributes: new DrupalAttribute(),
            label: "Stryker School of Medicine",
            href: "https://med.wmich.edu/",
            type: "link",
          },
        ],
      },
    },
    {
      attributes: new DrupalAttribute(),
      label: "Services",
      href: "",
      type: "nolink",
      children: {
        id: "submenu-services",
        attributes: new DrupalAttribute().setAttribute(
          "id",
          "submenu-services"
        ),
        items: [
          {
            attributes: new DrupalAttribute(),
            label: "Campus Safety Information and Resources",
            href: "https://wmich.edu/campus-safety",
            type: "link",
          },
          {
            attributes: new DrupalAttribute(),
            label: "Careers and Internships",
            href: "https://wmich.edu/career",
            type: "link",
          },
          {
            attributes: new DrupalAttribute(),
            label: "Human Resources",
            href: "https://wmich.edu/hr",
            type: "link",
          },
          {
            attributes: new DrupalAttribute(),
            label: "Maps",
            href: "https://wmich.edu/maps",
            type: "link",
          },
          {
            attributes: new DrupalAttribute(),
            label: "Parking",
            href: "https://wmich.edu/parking",
            type: "link",
          },
        ],
      },
    },
  ],
  links: [
    {
      label: "westernmichu on Instagram",
      href: "https://www.instagram.com/westernmichu",
    },
    {
      label: "westernmichu on Facebook",
      href: "https://www.facebook.com/westernmichu/",
    },
    {
      label: "@westernmichu on Twitter",
      href: "https://twitter.com/WesternMichU",
    },
    {
      label: "westernmichu on LinkedIn",
      href: "https://www.linkedin.com/school/westernmichu",
    },
    {
      label: "@westernmichu on TikTok",
      href: "https://www.tiktok.com/@westernmichu",
    },
  ],
  colorMode: "auto",
};
