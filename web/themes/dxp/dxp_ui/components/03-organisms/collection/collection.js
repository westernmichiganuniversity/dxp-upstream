/**
 * @file
 * Behaviors for the collection component.
 */
import Splide from "@splidejs/splide";

((Drupal, once) => {
  /**
   * Mounts Splide to a collection.
   *
   * @param {HTMLElement} element
   *   The input element.
   */
  function splideMount(element) {
    const thumbnails = element.parentElement.querySelector(".thumbnails");
    const main = new Splide(element, {
      type: thumbnails ? "fade" : "loop",
      arrows: !thumbnails,
      focus: thumbnails ? 1 : "center",
      pagination: false,
      rewind: true,
      keyboard: true,
      fixedWidth: thumbnails ? "100%" : 376,
      gap: thumbnails ? 0 : "1.5rem",
      updateOnMove: true,
      breakpoints: {
        640: {
          arrows: true,
          fixedWidth: thumbnails ? "100%" : 320,
        },
      },
    });
    if (thumbnails) {
      const thumbnailsSplide = new Splide(thumbnails, {
        fixedWidth: 251,
        gap: 6,
        rewind: true,
        pagination: false,
        isNavigation: true,
        updateOnMove: true,
        breakpoints: {
          640: {
            fixedWidth: 150,
            destroy: true,
          },
        },
      });
      main.sync(thumbnailsSplide);
      main.mount();
      thumbnailsSplide.mount();
    } else {
      main.mount();
    }
  }

  /**
   * Initialize Splide.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches behaviors to text fields.
   */
  Drupal.behaviors.collection = {
    attach(context) {
      once("dxp-collection", ".splide:not(.thumbnails)", context).forEach(
        splideMount
      );
    },
  };
})(Drupal, once);
