import DrupalAttribute from "drupal-attribute";
import CollectionTwig from "./collection.twig";
import { Image } from "../../01-atoms/image/image.stories";
import { Card } from "../../02-molecules/card/card.stories";
import { Link } from "../../01-atoms/link/link.stories";
import { Slide } from "../slide/slide.stories";
import item0 from "./item0.webp";
import item1 from "./item1.webp";
import item2 from "./item2.webp";
import item3 from "./item3.webp";
import "./collection";

export default {
  title: "Organisms/Collection",
  argTypes: {
    numOfItems: {
      control: {
        type: "number",
        min: 1,
        max: 12,
        step: 1,
      },
    },
    variant: {
      control: { type: "select" },
      options: ["card", "carousel", "gallery", "list", "split"],
    },
    aspectRatio: {
      control: { type: "select" },
      options: ["2:3", "1:1", "4:3", "3:2", "16:9", "auto"],
    },
    orientation: {
      control: { type: "radio" },
      options: ["horizontal", "vertical"],
    },
    size: {
      control: { type: "radio" },
      options: ["sm", "md", "lg"],
    },
    withActionLinks: { type: "boolean" },
    numOfLinksShown: {
      control: {
        type: "number",
        min: 0,
        max: 2,
        step: 1,
      },
    },
    alternating: { type: "boolean" },
    autoAdvance: { type: "boolean" },
    overlayCaption: { type: "boolean" },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();

  const items = [];
  let item;
  let itemMedia;
  let itemLabel;
  let itemDetail;
  let itemContent;
  let itemLink1Href;
  let itemLink1Label;
  let itemLink2Href;
  let itemLink2Label;
  let thumbnails;
  for (let i = 0; i < args.numOfItems; i++) {
    switch (i) {
      case 0:
        itemMedia = Image({
          ...Image.args,
          src: item0,
          alt: "Student at the beach",
        });
        itemLabel = "We are here for you";
        itemDetail = "Student Life";
        itemContent =
          "Hours and locations for Residence Life, Rec Center, Health Promotion, Counseling, Dining, Food Pantry and others.";
        itemLink1Href = "https://wmich.edu/students/resources";
        itemLink1Label = "View services";
        itemLink2Href = "https://wmich.edu/students/contact";
        itemLink2Label = "Contact us";
        break;

      case 1:
        itemMedia = Image({
          ...Image.args,
          src: item1,
          alt: "Student at Miller Fountain",
        });
        itemLabel = "Don’t go it alone";
        itemDetail = "Counseling Services";
        itemContent =
          "Students are returning to campus with higher rates of anxiety, burnout, and depression.";
        itemLink1Href = "https://wmich.edu/healthcenter/counseling/letstalk";
        itemLink1Label = "Make an appointment";
        itemLink2Href = "https://wmich.edu/healthcenter/immediate";
        itemLink2Label = "Get immediate help";
        break;

      case 2:
        itemMedia = Image({
          ...Image.args,
          src: item2,
          alt: "Student in F45",
        });
        itemLabel = "Take care";
        itemDetail = "YOU at Western";
        itemContent =
          "Transitioning to campus, for the first time or for returners, is especially hard right now.";
        itemLink1Href = "https://you.wmich.edu/";
        itemLink1Label = "Support your wellbeing";
        itemLink2Href = "https://wmich.edu/you";
        itemLink2Label = "About YOU at Western";
        break;

      case 3:
        itemMedia = Image({
          ...Image.args,
          src: item3,
          alt: "Esports",
        });
        itemLabel = "Leave your mark";
        itemDetail = "ExperienceWMU";
        itemContent =
          "WMU has hundreds of student organizations for students to discover and pursue their passions.";
        itemLink1Href = "https://experiencewmu.wmich.edu/organizations";
        itemLink1Label = "Find your fit";
        itemLink2Href = "https://experiencewmu.wmich.edu/events";
        itemLink2Label = "Browse student events";
        break;

      default:
        itemMedia = Image({ ...Image.args });
        itemLabel = "Item title";
        itemDetail = "Additional detail";
        itemContent =
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
        itemLink1Href = "https://wmich.edu";
        itemLink1Label = "Link 1";
        itemLink2Href = "https://wmich.edu";
        itemLink2Label = "Link 2";
    }

    if (args.numOfLinksShown < 1) {
      itemLink1Href = null;
      itemLink1Label = null;
    }
    if (args.numOfLinksShown < 2) {
      itemLink2Href = null;
      itemLink2Label = null;
    }

    item = {
      media: itemMedia,
      mediaAspect: args.aspectRatio,
      label: itemLabel,
      level: 2,
      detail: itemDetail,
      content: itemContent,
      link1Href: itemLink1Href,
      link1Label: itemLink1Label,
      link2Href: itemLink2Href,
      link2Label: itemLink2Label,
      size: args.size,
      variant: args.orientation,
      withActionLinks: args.withActionLinks,
      colorMode: args.colorMode,
    };

    switch (args.variant) {
      case "card":
        items.push(Card({ ...item }));
        break;

      case "carousel":
        items.push(Card({ ...item }));
        break;

      case "gallery":
        item = {
          media: itemMedia,
          mediaAspect: args.aspectRatio,
          heading: itemLabel,
          headingLevel: 2,
          text: itemContent,
          link1Href: itemLink1Href,
          link1Label: itemLink1Label,
          link2Href: itemLink2Href,
          link2Label: itemLink2Label,
          links: [],
          overlayCaption: true,
          halign: "right",
          valign: "bottom",
          width: "full",
        };
        items.push(Slide({ ...item }));
        thumbnails = [
          { url: item0, heading: "We are here for you" },
          { url: item1, heading: "Don't go it alone" },
          { url: item2, heading: "Take care" },
          { url: item3, heading: "Leave your mark" },
        ];
        break;

      case "split":
        break;

      case "list":
        item = {
          label: itemLabel,
          href: itemLink1Href,
          variant: "default",
          colorMode: args.colorMode,
        };
        items.push(Link({ ...item }));
        break;

      default:
    }
  }

  return CollectionTwig({ ...args, attributes, items, thumbnails });
};

export const CardsVertical = Template.bind({});
CardsVertical.args = {
  numOfItems: 4,
  numOfLinksShown: 2,
  variant: "card",
  aspectRatio: "3:2",
  orientation: "vertical",
  size: "sm",
  withActionLinks: true,
  colorMode: "auto",
};

export const CardsHorizontal = Template.bind({});
CardsHorizontal.args = {
  ...CardsVertical.args,
  orientation: "horizontal",
  size: "lg",
  withActionLinks: false,
};

export const Carousel = Template.bind({});
Carousel.args = {
  numOfItems: 4,
  numOfLinksShown: 1,
  variant: "carousel",
  aspectRatio: "3:2",
  orientation: "vertical",
  size: "md",
  withActionLinks: false,
  colorMode: "auto",
};

export const Gallery = Template.bind({});
Gallery.args = {
  numOfItems: 4,
  variant: "gallery",
  aspectRatio: "16:9",
  autoAdvance: true,
  overlayCaption: true,
  thumbnails: [],
};

export const List = Template.bind({});
List.args = {
  numOfItems: 4,
  numOfLinksShown: 1,
  variant: "list",
  colorMode: "auto",
};
