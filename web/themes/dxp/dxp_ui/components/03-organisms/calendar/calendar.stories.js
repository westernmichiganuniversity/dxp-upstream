import DrupalAttribute from "drupal-attribute";
import CalendarTwig from "./calendar.twig";
import { Image } from "../../01-atoms/image/image.stories";
import item0 from "../collection/item0.webp";
import item1 from "../collection/item1.webp";
import item2 from "../collection/item2.webp";
import item3 from "../collection/item3.webp";

export default {
  title: "Organisms/Calendar",
  argTypes: {
    variant: {
      control: { type: "select" },
      options: ["expanded", "compact"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const labelAttributes = new DrupalAttribute();
  const items = [
    {
      media: Image({ ...Image.args, src: item0, alt: "Student at the beach" }),
      heading:
        "Child Welfare Series 2022 - Family Preservation Using MST Theory and Practice Skills",
      date: "Nov. 4",
      time: "8:00am",
      body: "This webinar will explore the theoretical underpinnings of MST as a family preservation model. Information will be designed to give the human service worker some applicable skills to assess family systems.",
      url: "https://wmich.edu/events/68768",
    },
    {
      media: Image({
        ...Image.args,
        src: item1,
        alt: "Student at Miller Fountain",
      }),
      heading:
        "TLC Brown Bag Talk: Intercultural Communication Competence and International Chinese Education",
      date: "Nov. 4",
      time: "noon",
      location: "4510 Sangren Hall",
      body: "Join us for a Brown Bag Talk given by the 2021 TLC Faculty Research Grant winner Dr. Xiaojun Wang, Professor of Chinese, Department of World Languages and Literatures, WMU, and his research team.",
      url: "https://wmich.edu/events/69505",
    },
    {
      media: Image({ ...Image.args, src: item2, alt: "Student in F45" }),
      heading: "Discovery Acceleration Workshop: Open Access Publishing",
      date: "Monday, Nov. 7",
      time: "noon to 1 p.m.",
      body: "Open access is a movement advocating for research to be unrestricted and freely available to all. In this workshop we will present an overview of open access as well as discuss options for openly sharing research, data, and educational resources. We’ll also discuss the specifics of open access publishing and what the advantages and disadvantages are of choosing to publish research as open access. We’ll help you to distinguishing between true open access and “predatory” publishers.",
      url: "https://wmich.edu/events/68764",
    },
    {
      media: Image({ ...Image.args, src: item3, alt: "Esports" }),
      heading: "WMU Theatre presents The Thanksgiving Play",
      date: "Monday, Nov. 7",
      time: "2 p.m.",
      location: "Gilmore Theatre Complex, Williams Theatre",
      body:
        "Good intentions collide with absurd assumptions in Larissa FastHorse’s wickedly funny satire, as a troupe of terminally “woke” teaching artists scrambles to create a pageant that somehow manages to celebrate both Turkey Day and Native American Heritage Month.\n" +
        "\n" +
        "“This clever satire is something for which to be truly thankful.” – Hollywood Reporter",
      url: "https://wmich.edu/events/68713",
    },
  ];

  return CalendarTwig({ ...args, attributes, labelAttributes, items });
};

export const Calendar = Template.bind({});
Calendar.args = {
  variant: "expanded",
  level: 3,
  colorMode: "auto",
};
