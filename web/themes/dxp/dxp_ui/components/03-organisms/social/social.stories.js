import DrupalAttribute from "drupal-attribute";
import SocialTwig from "./social.twig";

export default {
  title: "Organisms/Social Links",
  argTypes: {
    links: { control: { type: "object" } },
    variant: {
      control: { type: "select" },
      options: ["horizontal", "vertical"],
    },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const links = [];
  args.links.forEach((item) => {
    links.push({
      attributes: new DrupalAttribute(),
      iconAttributes: new DrupalAttribute(),
      label: item.label,
      href: item.href,
    });
  });
  return SocialTwig({ ...args, attributes, links });
};

export const Horizontal = Template.bind({});
Horizontal.args = {
  links: [
    {
      label: "(269) 387-1000",
      href: "tel:+12693871000",
    },
    {
      label: "Email Us",
      href: "https://wmich.edu/contact",
    },
    {
      label: "westernmichu on Facebook",
      href: "https://www.facebook.com/westernmichu/",
    },
    {
      label: "westernmichu on Instagram",
      href: "https://www.instagram.com/westernmichu",
    },
    {
      label: "westernmichu on LinkedIn",
      href: "https://www.linkedin.com/school/westernmichu",
    },
    {
      label: "westernmichu on Snapchat",
      href: "https://www.snapchat.com/add/westernmichu",
    },
    {
      label: "wmuchoirs on SoundCloud",
      href: "https://soundcloud.com/wmuchoirs",
    },
    {
      label: "@westernmichu on TikTok",
      href: "https://www.tiktok.com/@westernmichu?",
    },
    {
      label: "@westernmichu on Twitter",
      href: "https://twitter.com/WesternMichU",
    },
    {
      label: "HIGE on Weibo",
      href: "http://www.weibo.com/p/1002063166553493/home?from=page_100206&mod=TAB",
    },
    {
      label: "westernmichu on YouTube",
      href: "https://www.youtube.com/westernmichu",
    },
  ],
  variant: "horizontal",
  size: "xl",
  colorMode: "auto",
};

export const Vertical = Template.bind({});
Vertical.args = {
  ...Horizontal.args,
  variant: "vertical",
  size: "base",
};
