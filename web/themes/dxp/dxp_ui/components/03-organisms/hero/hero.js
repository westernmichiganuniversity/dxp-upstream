/**
 * @file
 * Behaviors for the hero component.
 */

/**
 * This function creates an <iframe> (and YouTube player)
 * after the API code downloads.
 */
function onYouTubeIframeAPIReady() {
  const videos = document.querySelectorAll(
    '[data-provider="youtube"][data-video-id]'
  );
  videos.forEach((video) => {
    const vid = video.getAttribute("data-video-id");
    // eslint-disable-next-line no-undef
    const player = new YT.Player(vid, {
      videoId: vid,
      playerVars: {
        autoplay: 1,
        controls: 0,
        disablekb: 1,
        fs: 0,
        loop: 1,
        modestbranding: 1,
        playsinline: 1,
        playlist: vid,
      },
      events: {
        onReady: (event) => {
          player.mute();
          player.playVideo();
        },
        onStateChange: (event) => {
          // eslint-disable-next-line no-undef
          if (event.data === YT.PlayerState.PLAYING) {
            document.getElementById(vid).style.pointerEvents = "none";
          }
          if (
            // eslint-disable-next-line no-undef
            event.data === YT.PlayerState.UNSTARTED ||
            // eslint-disable-next-line no-undef
            event.data === YT.PlayerState.PAUSED
          ) {
            document.getElementById(vid).style.pointerEvents = "all";
          }
        },
      },
    });
  });
}

Drupal.behaviors.hero = {
  attach(context) {
    // once("vimeo", "div[data-vimeo-id]", context).forEach((wrapper) => {
    context.querySelectorAll("div[data-vimeo-id]").forEach((wrapper) => {
      const options = {
        id: wrapper.getAttribute("data-vimeo-id"),
        autopause: false,
        autoplay: true,
        background: true,
        controls: false,
        loop: true,
        muted: true,
        speed: false,
        byline: false,
        color: "f1c500",
        portrait: false,
        title: false,
        transparent: false,
      };
      // eslint-disable-next-line no-undef
      const player = new Vimeo.Player(wrapper, options);
      player.on("loaded", () => {
        player.setVolume(0);
        player.setLoop(true);
        player.play();
      });
    });
  },
};
