import DrupalAttribute from "drupal-attribute";
import HeroTwig from "./hero.twig";
import { Image } from "../../01-atoms/image/image.stories";
import { Callout } from "../../02-molecules/callout/callout.stories";

export default {
  title: "Organisms/Hero",
  argTypes: {
    halign: {
      control: { type: "select" },
      options: ["left", "center", "right"],
    },
    valign: {
      control: { type: "select" },
      options: ["top", "middle", "bottom"],
    },
    width: {
      control: { type: "select" },
      options: ["full", "3/4", "2/3", "1/2", "1/3", "1/4"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const captionAttributes = new DrupalAttribute();
  return HeroTwig({ attributes, captionAttributes, ...args });
};

export const Hero = Template.bind({});
Callout.args.colorMode = "dark";
Hero.args = {
  media: Image({ ...Image.args }),
  callout: Callout({ ...Callout.args }),
};
