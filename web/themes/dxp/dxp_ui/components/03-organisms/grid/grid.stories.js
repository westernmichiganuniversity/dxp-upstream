import DrupalAttribute from "drupal-attribute";
import GridTwig from "./grid.twig";

export default {
  title: "Organisms/Grid",
  argTypes: {
    layout: {
      control: { type: "select" },
      options: ["1:1", "1:2", "2:1", "1:1:1"],
    },
    items: { control: { type: "object" } },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  return GridTwig({ ...args, attributes });
};

export const Grid = Template.bind({});
Grid.args = {
  layout: "1:1",
  items: [
    '<div class="tw-prose tw-mx-auto tw-mb-8 dark:tw-prose-dark md:tw-prose-lg">' +
      "<h2>Career services and advising on hand</h2>" +
      "<p>Master’s degree candidates' career development goals vary greatly. The reasons for pursuing a graduate degree may include:</p>" +
      "<ul><li>Exploring career pathways and deciding to continue education.</li>" +
      "<li>&nbsp;Seeking a job and looking for a credential to aid in the search.</li>" +
      "<li>Looking for a career change and have a non-business undergraduate degree.</li>" +
      "<li>Seeking career advancement within current occupation or industry.</li>" +
      "</ul><p>Career advising is available for MBA and M.S.A. candidates, drop-in or by appointment.</p>" +
      '<ul><li>Please register with the&nbsp;<a href="https://wwwstage.wmich.edu/business/career">Zhang Career Center</a>.</li>' +
      "<li>Complete your profile and choose the MBA or M.S.A. program, and expected graduation month and year. You will be emailed when there are internship or professional employment opportunities for MBA and M.S.A. candidates.</li>" +
      '</ul><p>Meet with a career advisor who can assist you with career assessment, job searching and networking, as well as transforming your undergraduate resume into an executive resume.&nbsp; Flexible appointment times, including early evening, are available. Email&nbsp;<a href="mailto:careercenter-hcob@wmich.edu">careercenter-hcob@wmich.edu</a>&nbsp;to request an appointment, or visit the website for drop-in hours.</p>' +
      "</div>",
    '<div class="tw-prose tw-mx-auto tw-mb-8 dark:tw-prose-dark md:tw-prose-lg">' +
      "<h2>Study abroad with the Haworth College of Business</h2>" +
      "<p>We strongly encourage our graduate students to take advantage of the summer study abroad programs offered through the Haworth College of Business. You can earn from 3 to 6 credit hours in a two- to four-week time period. The study abroad credits count as electives in your MBA. There are several summer programs from which to choose with an all-inclusive approximate cost of $5,000–$6,000. Scholarships up to $1,000 are available to all business graduate students.</p>" +
      '<p>Students should contact&nbsp;<a href="https://wwwstage.wmich.edu/email/node/19762/field_email?destination=node/19762">Barb Caras-Tomczak</a>&nbsp;in the Global Business Center, located in 2320 Schneider Hall, for information on study abroad programs, scholarships and general information about the application process. Walk-in hours are Monday and Wednesday 8 a.m. to noon and 1 p.m. to 6:30 p.m., Tuesday and Thursday 1 p.m. to 6:30 p.m. and Friday 1 p.m. to 5 p.m.</p>' +
      '<p>Call (269) 387-5086 or email:&nbsp;<a href="mailto:mba-advising@wmich.edu">mba-advising@wmich.edu</a>&nbsp;for more information.</p>' +
      "</div>",
    '<div class="tw-prose tw-mx-auto tw-mb-8 dark:tw-prose-dark md:tw-prose-lg">' +
      "<h2>Lorem ipsum</h2>" +
      "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>" +
      "</div>",
  ],
};
