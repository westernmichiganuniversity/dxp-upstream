import DrupalAttribute from "drupal-attribute";
import SectionTwig from "./section.twig";
import { Text } from "../../01-atoms/text/text.stories";

export default {
  title: "Organisms/Section",
  argTypes: {
    label: { control: "text" },
    level: {
      control: { type: "select" },
      options: [1, 2, 3, 4, 5, 6],
    },
    useHeadingElement: { control: { type: "boolean" } },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const labelAttributes = new DrupalAttribute();
  const children = args.children.join("");
  return SectionTwig({ ...args, attributes, labelAttributes, children });
};

const sampleText = Text({
  ...Text.args,
  withMargin: true,
  content:
    "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id diam mi. Sed quis velit non elit euismod pellentesque. Aliquam et felis dapibus risus dictum dapibus in sed arcu.</p>",
});

export const Section = Template.bind({});
Section.args = {
  label: "A section containing text",
  children: [sampleText],
  useHeadingElement: true,
};

export const NestedSections = Template.bind({});
NestedSections.args = {
  label: "Heading 1",
  level: 1,
  useHeadingElement: true,
  children: [
    sampleText,
    Section({
      label: "Heading 2",
      level: 2,
      useHeadingElement: true,
      children: [
        sampleText,
        Section({
          label: "Heading 3",
          level: 3,
          useHeadingElement: true,
          children: [
            sampleText,
            Section({
              label: "Heading 4",
              level: 4,
              useHeadingElement: true,
              children: [
                sampleText,
                Section({
                  label: "Heading 5",
                  level: 5,
                  useHeadingElement: true,
                  children: [
                    sampleText,
                    Section({
                      label: "Heading 6",
                      level: 6,
                      useHeadingElement: true,
                      children: [sampleText],
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
      ],
    }),
  ],
};
