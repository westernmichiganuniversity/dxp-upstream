import DrupalAttribute from "drupal-attribute";
import AsideTwig from "./aside.twig";
import { Text } from "../../01-atoms/text/text.stories";

export default {
  title: "Organisms/Aside",
  argTypes: {
    label: { control: "text" },
    level: {
      control: { type: "select" },
      options: [1, 2, 3, 4, 5, 6],
    },
    float: {
      control: { type: "select" },
      options: ["none", "left", "right"],
    },
    anchor: { control: "text" },
    size: {
      control: { type: "select" },
      options: [
        "full",
        "prose",
        "7xl",
        "6xl",
        "5xl",
        "4xl",
        "3xl",
        "2xl",
        "xl",
        "lg",
        "md",
        "sm",
        "xs",
      ],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const labelAttributes = new DrupalAttribute();
  const children = args.children.join("");
  return AsideTwig({ ...args, attributes, labelAttributes, children });
};

const sampleText = Text({
  ...Text.args,
  withMargin: false,
  content:
    "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id diam mi. Sed quis velit non elit euismod pellentesque. Aliquam et felis dapibus risus dictum dapibus in sed arcu.</p>",
});

export const Aside = Template.bind({});
Aside.args = {
  label: "An aside containing text",
  level: 3,
  children: [sampleText],
  float: "right",
};
