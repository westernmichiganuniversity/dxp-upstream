/**
 * @file
 * Behaviors for header component.
 */
((Drupal, once) => {
  const body = document.querySelector("body");
  const container = document.querySelector("[data-menu]");
  const menu = document.querySelector("[data-menu] > div.tw-bg-white");
  const pageMenu = document.querySelector("[data-menu-page]");
  const overlay = document.querySelector("[data-menu-overlay]");
  const toggleMenuButton = document.querySelectorAll("[data-menu-button]")[0];
  const toggleMenuButton2 = document.querySelectorAll("[data-menu-button]")[1];
  const searchButton = document.querySelectorAll("[data-search-button]")[0];
  const searchButton2 = document.querySelectorAll("[data-search-button]")[1];
  const closeMenuButton = document.querySelector("[data-menu-close-button]");
  const focusableElements =
    'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
  const focusableContent = container.querySelectorAll(focusableElements);
  const firstFocusableElement = focusableContent[0];
  const lastFocusableElement = focusableContent[focusableContent.length - 1];
  const searchBox = document.querySelector('input[type="search"]');
  if (!document.querySelector("[data-menu-page] ul")) {
    pageMenu.classList.add("tw-hidden");
  }
  function toggleMenu() {
    const expanded =
      toggleMenuButton.getAttribute("aria-expanded") === "true" || false;
    if (expanded) {
      toggleMenuButton.removeAttribute("aria-expanded");
      toggleMenuButton2.removeAttribute("aria-expanded");
      searchButton.removeAttribute("aria-expanded");
      searchButton2.removeAttribute("aria-expanded");
      menu.classList.add("tw-translate-x-full");
    } else {
      toggleMenuButton.setAttribute("aria-expanded", "true");
      toggleMenuButton2.setAttribute("aria-expanded", "true");
      searchButton.setAttribute("aria-expanded", "true");
      searchButton2.setAttribute("aria-expanded", "true");
      container.classList.add("tw-visible");
      container.classList.remove("tw-invisible");
      menu.classList.remove("tw-translate-x-full");
    }
    overlay.classList.toggle("tw-opacity-0");
    overlay.classList.toggle("tw-opacity-75");
    closeMenuButton.classList.toggle("tw-opacity-0");
    closeMenuButton.classList.toggle("tw-opacity-1");
    body.classList.toggle("tw-overflow-hidden");
  }
  toggleMenuButton.addEventListener("click", toggleMenu);
  toggleMenuButton2.addEventListener("click", toggleMenu);
  searchButton.addEventListener("click", toggleMenu);
  searchButton2.addEventListener("click", toggleMenu);
  closeMenuButton.addEventListener("click", toggleMenu);
  overlay.addEventListener("click", toggleMenu);
  menu.addEventListener("transitionend", function () {
    if (this.classList.contains("tw-translate-x-full")) {
      container.classList.remove("tw-visible");
      container.classList.add("tw-invisible");
      toggleMenuButton.focus();
    } else {
      container.classList.add("tw-visible");
      container.classList.remove("tw-invisible");
      searchBox.focus();
    }
  });
  window.addEventListener("keydown", function (e) {
    const isEscPressed = e.key === "Escape";
    if (!isEscPressed) {
      return;
    }
    if (toggleMenuButton.getAttribute("aria-expanded") === "true") {
      toggleMenuButton.click();
      toggleMenuButton.focus();
    }
  });
  document.addEventListener("keydown", function (e) {
    const isTabPressed = e.key === "Tab" || e.keyCode === 9;
    if (
      !isTabPressed ||
      toggleMenuButton.getAttribute("aria-expanded") !== "true"
    ) {
      return;
    }
    if (e.shiftKey) {
      if (document.activeElement === firstFocusableElement) {
        lastFocusableElement.focus();
        e.preventDefault();
      }
    } else if (document.activeElement === lastFocusableElement) {
      firstFocusableElement.focus();
      e.preventDefault();
    }
  });
})(Drupal, once);
