import DrupalAttribute from "drupal-attribute";
import HeaderTwig from "./header.twig";

export default {
  title: "Organisms/Header",
  argTypes: {
    items: { control: { type: "object" } },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const menuAttributes = new DrupalAttribute();
  return HeaderTwig({
    ...args,
    attributes,
    menuAttributes,
  });
};

export const Header = Template.bind({});
Header.args = {
  items: [
    {
      attributes: new DrupalAttribute(),
      label: "Apply",
      href: "https://wmich.edu/apply",
      type: "link",
    },
    {
      attributes: new DrupalAttribute(),
      label: "Give",
      href: "https://secure.wmualumni.org/s/give",
      type: "link",
    },
    {
      attributes: new DrupalAttribute(),
      label: "Visit",
      href: "https://wmich.edu/visit",
      type: "link",
    },
  ],
  colorMode: "auto",
};
