import Typography from "./typography.twig";

export default {
  title: "Design tokens/Typography",
  argTypes: {
    name: {
      table: {
        disable: true,
      },
    },
    class: {
      table: {
        disable: true,
      },
    },
    italic: {
      control: { type: "boolean" },
    },
    weight: {
      control: {
        type: "select",
        labels: {
          "tw-font-normal": "normal",
          "tw-font-medium": "medium",
          "tw-font-semibold": "semibold",
        },
      },
      options: ["tw-font-normal", "tw-font-medium", "tw-font-semibold"],
    },
  },
};

const glyphs = [
  ..."ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890‘’“”'\",.?!:;-–—_/\\|`~@#$%^&*()[]{}−+÷×<=>®©™",
];

const sample =
  "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Massa sed elementum tempus egestas sed sed risus pretium. Faucibus a pellentesque sit amet. Pellentesque sit amet porttitor eget dolor morbi non. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus.</p>\n" +
  "<p>Consectetur purus ut faucibus pulvinar elementum integer enim. Pretium fusce id velit ut tortor pretium viverra suspendisse potenti. Netus et malesuada fames ac turpis egestas sed. Pharetra pharetra massa massa ultricies mi quis. Eget egestas purus viverra accumsan in nisl nisi scelerisque. Neque gravida in fermentum et sollicitudin ac orci. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Ut lectus arcu bibendum at varius vel. Leo in vitae turpis massa sed elementum. Dignissim cras tincidunt lobortis feugiat vivamus at. Massa enim nec dui nunc. Integer malesuada nunc vel risus commodo viverra. Libero volutpat sed cras ornare.</p>";

const Template = ({ ...args }) => {
  return Typography({ glyphs, ...args });
};

export const Montserrat = Template.bind({});
Montserrat.args = {
  name: "Montserrat",
  class: "tw-font-sans",
  italic: false,
  weight: "tw-font-normal",
  sample,
};

export const CooperBlack = Template.bind({});
CooperBlack.args = {
  name: "Cooper Black",
  class: "tw-font-cooper-black",
  italic: false,
  sample,
};
CooperBlack.parameters = { controls: { exclude: ["weight"] } };

export const CourierNew = Template.bind({});
CourierNew.args = {
  name: "Courier New",
  class: "tw-font-mono",
  italic: false,
  weight: "tw-font-normal",
  sample,
};
