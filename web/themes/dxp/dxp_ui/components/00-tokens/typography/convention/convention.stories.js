import DrupalAttribute from "drupal-attribute";
import Convention from "./convention.twig";
import { Element } from "../../../02-molecules/form/element/element.stories";

export default {
  title: "Design tokens/Typography/Conventions",
  argTypes: {
    name: { table: { disable: true } },
    description: { table: { disable: true } },
    sample: { table: { disable: true } },
  },
};

const samplePlaceholder = Element({
  ...Element.args,
  type: "email",
  label: "Email address",
  placeholder: "your.name@wmich.edu",
  containerClass: false,
});

const Template = ({ ...args }) => {
  const labelAttributes = new DrupalAttribute();
  const descriptionAttributes = new DrupalAttribute();
  const headingAttributes = new DrupalAttribute();
  const sampleAttributes = new DrupalAttribute();
  return Convention({
    ...args,
    labelAttributes,
    descriptionAttributes,
    headingAttributes,
    sampleAttributes,
  });
};

export const Bold = Template.bind({});
Bold.args = {
  name: "Bold",
  description:
    "<p>Bold is used to add hierarchy within a sentence or to call attention. It’s also used to directly refer to the names of UI elements in running text.</p>",
  sample:
    "<p>Please provide your <b>Full name</b> and your <b>Email address</b> in the form below.</p>",
};

export const Italic = Template.bind({});
Italic.args = {
  name: "Italic",
  description: "<p>Italic is used only for placeholder (“ghost”) text.</p>",
  sample: samplePlaceholder,
};

export const Underline = Template.bind({});
Underline.args = {
  name: "Underline",
  description:
    "<p>Underline is used only for text links (either hover state or default state, depending on the style of the link) and should never be used as a mechanism for adding emphasis.</p>",
  sample:
    "<p>See <a href='https://wmich.edu'>Privacy Policy</a> for more details</p>",
};

export const Strong = Template.bind({});
Strong.args = {
  name: "Strong",
  description:
    "<p>Strong can be used for placing importance on part of a sentence, rendering the text as a heavier font weight. This is for semantic formatting, when it’s intended to add a tone that conveys importance.</p>",
  sample: "<p>This email contains <strong>sensitive information</strong>.</p>",
};

export const Emphasis = Template.bind({});
Emphasis.args = {
  name: "Emphasis",
  description:
    "<p>Emphasis can be used for placing emphasis on part of a sentence, rendering the text as italic.</p>",
  sample: "<p>I <em>love</em> drawing with Photoshop brushes!</p>",
};

export const LineLength = Template.bind({});
LineLength.args = {
  name: "Line Length",
  description:
    "<p>Paragraphs of text that are too long are difficult to follow. Ideally, blocks of text should be roughly 80 characters wide.</p>",
  sample:
    "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Massa sed elementum tempus egestas sed sed risus pretium. Faucibus a pellentesque sit amet. Pellentesque sit amet porttitor eget dolor morbi non. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus.</p><p>Consectetur purus ut faucibus pulvinar elementum integer enim. Pretium fusce id velit ut tortor pretium viverra suspendisse potenti. Netus et malesuada fames ac turpis egestas sed. Pharetra pharetra massa massa ultricies mi quis. Eget egestas purus viverra accumsan in nisl nisi scelerisque. Neque gravida in fermentum et sollicitudin ac orci. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Ut lectus arcu bibendum at varius vel. Leo in vitae turpis massa sed elementum. Dignissim cras tincidunt lobortis feugiat vivamus at. Massa enim nec dui nunc. Integer malesuada nunc vel risus commodo viverra. Libero volutpat sed cras ornare.</p>",
};
