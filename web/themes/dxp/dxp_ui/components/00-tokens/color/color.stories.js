import chroma from "chroma-js";
import DrupalAttribute from "drupal-attribute";
import tailwindColors from "../../../../../../../tailwind.colors";
import Color from "./color.twig";

const modifiers = [50, 100, 200, 300, 400, 500, 600, 700, 800, 900];
const regexpHsl = /hsl\(([0-9]+), ?([0-9.]+)%, ?([0-9.]+)%\)/gm;

export default {
  title: "Design tokens/Color",
  argTypes: {
    displayColorValueAs: {
      control: { type: "radio" },
      options: ["hex", "hsl"],
      defaultValue: "hex",
    },
    backgroundColor: {
      control: { type: "select" },
      options: Object.keys(tailwindColors),
      defaultValue: "white",
    },
    backgroundModifier: {
      control: { type: "select" },
      options: ["DEFAULT", ...modifiers],
      defaultValue: "DEFAULT",
    },
    darkModeBackgroundColor: {
      control: { type: "select" },
      options: Object.keys(tailwindColors),
      defaultValue: "gray",
    },
    darkModeBackgroundModifier: {
      control: { type: "select" },
      options: ["DEFAULT", ...modifiers],
      defaultValue: 800,
    },
  },
};

/**
 * Finds the modifier that is the same as the given color's DEFAULT.
 *
 * @param {Object|string} tailwindColor
 *   A TailwindCSS color definition. Can be a color value string or an object
 *   where the keys are modifiers and the values are color value strings.
 * @return {string}
 *   The default modifier. For example, if tailwindColor.DEFAULT is the same
 *   as tailwindColor.400, returns 400. If tailwindColor is a string or has no
 *   default modifier, returns an empty string.
 */
const findDefaultModifier = (tailwindColor) => {
  let defaultModifier = "";
  if (typeof tailwindColor === "object" && "DEFAULT" in tailwindColor) {
    Object.keys(tailwindColor).forEach((modifier) => {
      if (
        modifier !== "DEFAULT" &&
        tailwindColor[modifier] === tailwindColor.DEFAULT
      ) {
        defaultModifier = modifier;
      }
    });
  }
  return defaultModifier;
};

/**
 * Converts a color value to hex.
 *
 * @param {string} color
 *   A color written in either HSL or hex.
 * @return {string}
 *   The color written in hex.
 */
const getHex = (color) => {
  const match = regexpHsl.exec(color);
  // If the color is written in HSL, convert it to hex.
  if (match) {
    return chroma({
      h: match[1],
      s: parseFloat(match[2]) / 100.0,
      l: parseFloat(match[3]) / 100.0,
    }).hex();
  }
  // Otherwise, it should already be a hex value.
  return color;
};

/**
 * Converts a color value to HSL.
 *
 * @param {string} color
 *   A color written in either HSL or hex.
 * @return {string}
 *   The color written in HSL.
 */
const getHsl = (color) => {
  const match = regexpHsl.exec(color);
  // If the color is written in HSL, return it.
  if (match) {
    return color;
  }
  // Otherwise, convert it to HSL.
  const hsl = chroma(color).hsl();
  hsl[0] = Math.round(Number.isNaN(hsl[0]) ? 0 : hsl[0]).toString();
  hsl[1] = Number((hsl[1] * 100.0).toFixed(1)).toString();
  hsl[2] = Number((hsl[2] * 100.0).toFixed(1)).toString();
  return `hsl(${hsl[0]}, ${hsl[1]}%, ${hsl[2]}%)`;
};

/**
 * Constructs a TailwindCSS background color class.
 *
 * @param {string} name
 *   The name of a color defined in the TailwindCSS config.
 * @param {string|integer} modifier
 *   (optional) A color modifier.
 * @return {string}
 *   A TailwindCSS background color class.
 */
const getBackgroundClass = (name, modifier = "") => {
  if (modifier && modifier !== "DEFAULT") {
    return `tw-bg-${name}-${modifier}`;
  }
  return `tw-bg-${name}`;
};

/**
 * Checks whether a contrast ratio meets WCAG 2.1 standards.
 *
 * @param {float} ratio
 *   A color contrast ratio.
 * @param {string} standard
 *   The WCAG standard to use, either "AA" or "AAA".
 * @param {boolean} darkMode
 *   (optional) Whether to use dark mode styles.
 * @return {{classes: (string), ratio, text: string}}
 *   classes: Classes to use for styling the text.
 *   text: One of the following values:
 *     - "Fail" if the contrast is insufficient
 *     - "Large" if the contrast is only sufficient for large test
 *     - "Pass" if the contrast is sufficient for all text.
 */
const contrastWcag = (ratio, standard, darkMode = false) => {
  const pass = standard === "AAA" ? 7.0 : 4.5;
  const large = standard === "AAA" ? 4.5 : 3.0;

  let text = "Fail";
  let classes = darkMode
    ? "tw-font-medium tw-text-negativeDarkMode"
    : "tw-font-medium tw-text-negative";
  if (ratio >= pass) {
    text = "Pass";
    classes = darkMode ? "tw-text-gray-50" : "tw-text-gray-900";
  } else if (ratio >= large) {
    text = "Large";
    classes = darkMode ? "tw-text-lilac-300" : "tw-text-lilac-600";
  }
  return { classes, ratio, text };
};

/**
 * Generates color contrast test results.
 *
 * @param {string} foreground
 *   The foreground color, in hex format.
 * @param {string} background
 *   The background color, in hex format.
 * @param {string} darkModeBackground
 *   The dark mode background color, in hex format.
 * @return {Object[]}
 *   An array of test results:
 *   - standard: The WCAG standard tested, either "AA" or "AAA".
 *   - light: The contrastWcag() results for light mode.
 *   - dark: The contrastWcag() results for dark mode.
 */
const calculateContrast = (foreground, background, darkModeBackground) => {
  const contrast = chroma.contrast(foreground, background).toFixed(2);
  const darkModeContrast = chroma
    .contrast(foreground, darkModeBackground)
    .toFixed(2);
  const result = [];
  ["AA", "AAA"].forEach((standard) => {
    result.push({
      standard,
      light: contrastWcag(contrast, standard),
      dark: contrastWcag(darkModeContrast, standard, true),
    });
  });
  return result;
};

/**
 * Build swatches from a list of TailwindCSS color names.
 *
 * @param {string[]} names
 *   Names of colors defined in the TailwindCSS config.
 * @return {Object[]}
 *   Color swatch data for color.twig.
 */
const buildSwatchesFromNames = (names) => {
  const swatches = [];
  names.forEach((name) => {
    swatches.push({
      hex: "",
      hsl: "",
      modifier: name,
      class: getBackgroundClass(name),
    });
  });
  return swatches;
};

/**
 * Build swatches from a TailwindCSS color definition.
 *
 * @param {string} name
 *   The name of the color as defined in the TailwindCSS config.
 * @param {Object|string} tailwindColor
 *   A TailwindCSS color definition. Can be a color value string or an object
 *   where the keys are modifiers and the values are color value strings.
 * @param {string} background
 *   The background color in hex format.
 * @param {string} darkModeBackground
 *   The dark mode background color in hex format.
 * @return {Object[]}
 *   Color swatch data for color.twig.
 */
const buildSwatchesFromColor = (
  name,
  tailwindColor,
  background,
  darkModeBackground
) => {
  const swatches = [];
  let swatch;
  if (typeof tailwindColor === "string") {
    swatch = {
      hex: getHex(tailwindColor),
      hsl: getHsl(tailwindColor),
      modifier: "",
      class: getBackgroundClass(name),
    };
    swatch.contrast = calculateContrast(
      swatch.hex,
      background,
      darkModeBackground
    );
    swatches.push(swatch);
  } else {
    Object.entries(tailwindColor).forEach(([modifier, color]) => {
      if (modifier !== "DEFAULT") {
        swatch = {
          hex: getHex(color),
          hsl: getHsl(color),
          modifier,
          class: getBackgroundClass(name, modifier),
        };
        swatch.contrast = calculateContrast(
          swatch.hex,
          background,
          darkModeBackground
        );
        swatches.push(swatch);
      }
    });
  }
  return swatches;
};

const Template = ({ ...args }) => {
  // Set the classes for the background colors.
  const background = getBackgroundClass(
    args.backgroundColor,
    args.backgroundModifier
  );
  const darkModeBackground = `dark:${getBackgroundClass(
    args.darkModeBackgroundColor,
    args.darkModeBackgroundModifier
  )}`;

  // Get the background colors in hex format.
  const backgroundHex =
    typeof tailwindColors[args.backgroundColor] === "string"
      ? getHex(tailwindColors[args.backgroundColor])
      : getHex(tailwindColors[args.backgroundColor][args.backgroundModifier]);
  const darkModeBackgroundHex =
    typeof tailwindColors[args.darkModeBackgroundColor] === "string"
      ? getHex(tailwindColors[args.darkModeBackgroundColor])
      : getHex(
          tailwindColors[args.darkModeBackgroundColor][
            args.darkModeBackgroundModifier
          ]
        );

  // Build the swatches.
  Object.values(args.colors).forEach((color) => {
    if (!Array.isArray(color.swatches)) {
      if (Array.isArray(color.values)) {
        color.swatches = buildSwatchesFromNames(color.values);
      } else {
        color.swatches = buildSwatchesFromColor(
          color.name,
          color.values,
          backgroundHex,
          darkModeBackgroundHex
        );
        color.default = findDefaultModifier(color.values);
      }
    }
  });

  const attributes = new DrupalAttribute();
  return Color({ ...args, background, darkModeBackground, attributes });
};

export const BrandGuide = Template.bind({});
BrandGuide.args = {
  name: "Brand Guide Color Palette",
  colors: [
    {
      name: "Primary",
      values: ["pms7406", "white", "pms4625"],
    },
    {
      name: "Neutral",
      values: ["pms401", "pms5497", "pms7531"],
    },
    {
      name: "Accent",
      values: ["pms3285", "pms1665", "pms200", "pms2567", "pms639", "pms383"],
    },
  ],
};

export const DesignSystem = Template.bind({});
DesignSystem.args = { name: "Design System Color Palette", colors: [] };
Object.entries(tailwindColors).forEach(([name, values]) => {
  DesignSystem.args.colors.push({
    name,
    values,
  });
});

export const Status = Template.bind({});
Status.args = {
  name: "Status Colors",
  colors: [
    {
      name: "Info",
      values: ["info", "infoDarkMode", "infoBackground"],
    },
    {
      name: "Notice",
      values: ["notice", "noticeDarkMode", "noticeBackground"],
    },
    {
      name: "Positive",
      values: ["positive", "positiveDarkMode", "positiveBackground"],
    },
    {
      name: "Negative",
      values: ["negative", "negativeDarkMode", "negativeBackground"],
    },
  ],
};
