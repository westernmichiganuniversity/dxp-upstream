import DrupalAttribute from "drupal-attribute";
import Iconography from "./iconography.twig";
import Icons from "./iconography.yml";

export default {
  title: "Design tokens/Iconography",
  argTypes: {
    name: {
      table: {
        disable: true,
      },
    },
    prefix: {
      table: {
        disable: true,
      },
    },
    icon: {
      table: {
        disable: true,
      },
    },
    uses: {
      table: {
        disable: true,
      },
    },
  },
};

const sizes = ["sm", "base", "lg", "xl", "2xl", "3xl", "4xl", "5xl"];
const getIcon = (key) => {
  let icon;
  if (Icons[key].uses) {
    icon = { ...getIcon(Icons[key].uses) };
    icon = Object.assign(icon, Icons[key]);
  } else {
    icon = Icons[key];
  }
  return icon;
};

const Template = ({ ...args }) => {
  const headingAttributes = new DrupalAttribute();
  return Iconography({ ...args, sizes, headingAttributes });
};

export const Academics = Template.bind({});
Academics.args = getIcon("Academics");

export const Apply = Template.bind({});
Apply.args = getIcon("Apply");

export const Calendar = Template.bind({});
Calendar.args = getIcon("Calendar");

export const Call = Template.bind({});
Call.args = getIcon("Call");

export const Contact = Template.bind({});
Contact.args = getIcon("Contact");

export const Cost = Template.bind({});
Cost.args = getIcon("Cost");

export const Dining = Template.bind({});
Dining.args = getIcon("Dining");

export const Download = Template.bind({});
Download.args = getIcon("Download");

export const Email = Template.bind({});
Email.args = getIcon("Email");

export const Global = Template.bind({});
Global.args = getIcon("Global");

export const Graduation = Template.bind({});
Graduation.args = getIcon("Graduation");

export const Health = Template.bind({});
Health.args = getIcon("Health");

export const Home = Template.bind({});
Home.args = getIcon("Home");

export const Hours = Template.bind({});
Hours.args = getIcon("Hours");

export const Information = Template.bind({});
Information.args = getIcon("Information");

export const Location = Template.bind({});
Location.args = getIcon("Location");

export const LogIn = Template.bind({});
LogIn.args = getIcon("LogIn");

export const Negative = Template.bind({});
Negative.args = getIcon("Negative");

export const Notice = Template.bind({});
Notice.args = getIcon("Notice");

export const Policy = Template.bind({});
Policy.args = getIcon("Policy");

export const Positive = Template.bind({});
Positive.args = getIcon("Positive");

export const PurchaseTickets = Template.bind({});
PurchaseTickets.args = getIcon("PurchaseTickets");

export const Research = Template.bind({});
Research.args = getIcon("Research");

export const Search = Template.bind({});
Search.args = getIcon("Search");

export const Transportation = Template.bind({});
Transportation.args = getIcon("Transportation");

export const Users = Template.bind({});
Users.args = getIcon("Users");

export const Visit = Template.bind({});
Visit.args = getIcon("Visit");

export const Facebook = Template.bind({});
Facebook.args = getIcon("Facebook");

export const Instagram = Template.bind({});
Instagram.args = getIcon("Instagram");

export const LinkedIn = Template.bind({});
LinkedIn.args = getIcon("LinkedIn");
LinkedIn.storyName = "LinkedIn";

export const Snapchat = Template.bind({});
Snapchat.args = getIcon("Snapchat");

export const SoundCloud = Template.bind({});
SoundCloud.args = getIcon("SoundCloud");
SoundCloud.storyName = "SoundCloud";

export const TikTok = Template.bind({});
TikTok.args = getIcon("TikTok");
TikTok.storyName = "TikTok";

export const Twitter = Template.bind({});
Twitter.args = getIcon("Twitter");

export const Weibo = Template.bind({});
Weibo.args = getIcon("Weibo");

export const YouTube = Template.bind({});
YouTube.args = getIcon("YouTube");
YouTube.storyName = "YouTube";
