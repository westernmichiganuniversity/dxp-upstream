import DrupalAttribute from "drupal-attribute";
import ObjectStyle from "./objectStyle.twig";
import { Button } from "../../01-atoms/button/button.stories";
import { TextField } from "../../01-atoms/textfield/textfield.stories";
import { Element } from "../../02-molecules/form/element/element.stories";

export default {
  title: "Design tokens/Object style",
  argTypes: {
    name: { table: { disable: true } },
    description: { table: { disable: true } },
    items: { table: { disable: true } },
  },
};

const sampleButton = Button({ ...Button.args, label: "Hello world!" });

const sampleCheckbox = Element({
  ...Element.args,
  type: "checkbox",
  label: "Checkbox",
  labelDisplay: "after",
  containerClass: false,
});

const sampleInput = TextField({
  ...TextField.args,
  placeholder: "Press Tab to see the keyboard focus decoration",
});

const Template = ({ ...args }) => {
  const labelAttributes = new DrupalAttribute();
  const descriptionAttributes = new DrupalAttribute();
  Object.values(args.items).forEach((item) => {
    item.headingAttributes = new DrupalAttribute();
    item.sampleAttributes = new DrupalAttribute();
  });
  return ObjectStyle({ ...args, labelAttributes, descriptionAttributes });
};

export const Rounding = Template.bind({});
Rounding.args = {
  name: "Rounding",
  description:
    "<p>Almost all components are rounded. Only specific components (e.g., the tip of a tooltip) are exceptions to this rule.</p>",
  items: [
    {
      heading: "Default rounding",
      sample: `<p>This is the rounding used by the majority of components.</p>${sampleButton}`,
    },
    {
      heading: "Small rounding",
      sample: `<p>A few components, like the checkbox, have a smaller rounding. In this case, the corner radius is connected to the thickness of its border width in order to have the inside of the border perfectly square and the outside perfectly rounded.</p>${sampleCheckbox}`,
    },
  ],
};

export const BordersAndRuleLines = Template.bind({});
BordersAndRuleLines.args = {
  name: "Borders and Rule Lines",
  description:
    "<p>Borders are used to outline the frames of components and to structure content. The border width remains the same for desktop scale and mobile scale.</p>",
  items: [
    {
      heading: "1px border",
      sample: `<p>The 1-pixel border is the most common border width. It is used for the borders of clickable/interactive elements (e.g., text fields, buttons, popovers) and as a content divider (e.g., tables, small rule lines).</p>${sampleButton}`,
    },
    {
      heading: "2px border",
      sample: `<p>A 2-pixel line is used for the keyboard focus decoration.</p>${sampleInput}`,
    },
    {
      heading: "4px border",
      sample: `<p>The 4-pixel border width is reserved for large dividers.</p><div class='tw-grid tw-grid-cols-3 tw-divide-x-4 tw-divide-gray-700 tw-divide-dotted'><div class='tw-text-center tw-px-4'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div><div class='tw-text-center tw-px-4'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div><div class='tw-text-center tw-px-4'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div></div>`,
    },
  ],
};
BordersAndRuleLines.storyName = "Borders and Rule Lines";

export const DropShadow = Template.bind({});
DropShadow.args = {
  name: "Drop Shadow",
  description:
    "<p>Shadows are reserved for transient components that appear elevated and are dismissible (e.g., dropdown menus).</p>",
  items: [
    {
      heading: "Default shadow",
      sample:
        "<p>The drop shadow opacity increases in dark mode to appear visually consistent with light mode.</p><ul class='tw-shadow-lg dark:tw-shadow-lg-dark tw-px-4 tw-py-2 tw-border tw-border-gray-700 dark:tw-border-gray-50 tw-inline-block'><li>Home</li><li>Documents</li><li>Gallery</li></ul>",
    },
  ],
};
