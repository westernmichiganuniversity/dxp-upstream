import DrupalAttributes from "drupal-attribute";
import ImageTwig from "./image.twig";
import DrupalImageTwig from "../../../templates/field/image.html.twig";
import horizontal from "./sample-horizontal.webp";
import vertical from "./sample-vertical.webp";
import square from "./sample-square.webp";
import small from "./kitten-small.png";
import medium from "./kitten-medium.png";
import large from "./kitten-large.png";

export default {
  title: "Atoms/Image",
  argTypes: {
    sources: { table: { disable: true } },
    src: { table: { disable: true } },
    alt: { control: { type: "text" } },
  },
};

const Template = ({ ...args }) => {
  const imgAttributes = new DrupalAttributes();
  imgAttributes.setAttribute("src", args.src);
  imgAttributes.setAttribute("alt", args.alt);
  const img = DrupalImageTwig({ attributes: imgAttributes });

  const sources = [];
  const knownAttributes = ["srcset", "media", "type"];
  let sourceAttributes;
  if (args.sources) {
    args.sources.forEach((source) => {
      sourceAttributes = new DrupalAttributes();
      knownAttributes.forEach((attr) => {
        if (source[attr]) {
          sourceAttributes.setAttribute(attr, source[attr]);
        }
      });
      sources.push(sourceAttributes);
    });
  }

  return ImageTwig({ img, sources });
};

export const Image = Template.bind({});
Image.args = {
  src: horizontal,
  alt: "Bronco sculpture",
};

export const Vertical = Template.bind({});
Vertical.args = {
  ...Image.args,
  src: vertical,
};

export const Square = Template.bind({});
Square.args = {
  ...Image.args,
  src: square,
};

export const ArtDirection = Template.bind({});
ArtDirection.args = {
  ...Image.args,
  alt: "A cute kitten",
  sources: [
    { srcset: small, media: "(max-width:734px)", type: "image/png" },
    { srcset: medium, media: "(max-width:1068px)", type: "image/png" },
    { srcset: large, media: "(min-width:0px)", type: "image/png" },
  ],
};
