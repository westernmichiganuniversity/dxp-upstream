import DrupalAttribute from "drupal-attribute";
import HeadingTwig from "./heading.twig";

export default {
  title: "Atoms/Heading",
  argTypes: {
    label: { control: { type: "text" } },
    level: {
      control: { type: "select" },
      options: [1, 2, 3, 4, 5, 6],
    },
    size: {
      control: { type: "select" },
      options: ["2xl", "xl", "lg", "md", "sm", "xs"],
    },
    weight: {
      control: { type: "select" },
      options: ["light", "medium", "heavy"],
    },
    color: {
      control: { type: "select" },
      options: ["brown", "gray"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
    withMargin: { control: { type: "boolean" } },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  return HeadingTwig({ attributes, ...args });
};

export const Heading = Template.bind({});
Heading.args = {
  label: "This is a heading",
};
