import IconTwig from "./icon.twig";
import IconLibrary from "../../00-tokens/iconography/iconography.yml";

const icons = () => {
  const iconList = [];
  Object.values(IconLibrary).forEach((icon) => {
    if (icon.icon) {
      iconList.push(icon.icon);
    }
  });
  return iconList;
};

export default {
  title: "Atoms/Icons/Icon",
  argTypes: {
    prefix: {
      control: { type: "select" },
      options: ["fas", "fab"],
    },
    icon: {
      control: { type: "select" },
      options: icons(),
    },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl", "2xl", "3xl", "4xl", "5xl"],
    },
  },
};

const Template = ({ ...args }) => {
  return IconTwig({ ...args });
};

export const Icon = Template.bind({});
Icon.args = {
  prefix: "fas",
  icon: "fa-book",
  size: "base",
};
