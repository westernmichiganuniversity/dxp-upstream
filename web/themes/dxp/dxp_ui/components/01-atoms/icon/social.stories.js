import DrupalAttribute from "drupal-attribute";
import SocialTwig from "./social.twig";

export default {
  title: "Atoms/Icons/Social Icon",
  argTypes: {
    variant: {
      control: { type: "select" },
      options: [
        "Call",
        "Email",
        "Facebook",
        "Instagram",
        "LinkedIn",
        "Snapchat",
        "SoundCloud",
        "TikTok",
        "Twitter",
        "Weibo",
        "YouTube",
      ],
    },
    label: { control: { type: "text" } },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl", "2xl", "3xl", "4xl", "5xl"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  return SocialTwig({ attributes, ...args });
};

export const SocialIcon = Template.bind({});
SocialIcon.args = {
  variant: "Facebook",
  size: "5xl",
};
