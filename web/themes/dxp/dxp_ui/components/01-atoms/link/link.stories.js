import DrupalAttribute from "drupal-attribute";
import LinkTwig from "./link.twig";

export default {
  title: "Atoms/Link",
  argTypes: {
    label: { control: { type: "text" } },
    href: { control: { type: "text" } },
    variant: {
      control: { type: "radio" },
      options: ["default", "menu"],
    },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  return LinkTwig({ ...args, attributes });
};

export const Link = Template.bind({});
Link.args = {
  label: "Label",
  href: "https://wmich.edu",
  variant: "default",
  size: "base",
  colorMode: "auto",
};
