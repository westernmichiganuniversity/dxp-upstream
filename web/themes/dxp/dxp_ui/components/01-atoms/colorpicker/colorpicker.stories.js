import DrupalAttribute from "drupal-attribute";
import ColorPickerTwig from "./colorpicker.twig";

export default {
  title: "Atoms/Color Picker",
  argTypes: {
    id: { table: { disable: true } },
    name: { table: { disable: true } },
    isDisabled: { control: { type: "boolean" } },
    hasErrors: { control: { type: "boolean" } },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const knownAttributes = ["id", "name"];
  knownAttributes.forEach((attr) => {
    if (args[attr]) {
      attributes.setAttribute(attr, args[attr]);
    }
  });
  return ColorPickerTwig({ attributes, ...args });
};

export const ColorPicker = Template.bind({});
ColorPicker.args = {
  id: "story",
  name: "story",
  isDisabled: false,
  hasErrors: false,
  size: "base",
  colorMode: "auto",
};
