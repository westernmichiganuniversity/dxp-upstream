import DrupalAttribute from "drupal-attribute";
import RadioTwig from "./radio.twig";

export default {
  title: "Atoms/Radio",
  argTypes: {
    id: { table: { disable: true } },
    name: { table: { disable: true } },
    isSelected: { control: { type: "boolean" } },
    isDisabled: { control: { type: "boolean" } },
    hasErrors: { control: { type: "boolean" } },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const knownAttributes = ["id", "name"];
  knownAttributes.forEach((attr) => {
    if (args[attr]) {
      attributes.setAttribute(attr, args[attr]);
    }
  });
  return RadioTwig({ attributes, ...args });
};

export const Radio = Template.bind({});
Radio.args = {
  id: "story",
  name: "story",
  isSelected: false,
  isDisabled: false,
  hasErrors: false,
  size: "base",
  colorMode: "auto",
};
