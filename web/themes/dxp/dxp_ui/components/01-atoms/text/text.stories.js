import DrupalAttribute from "drupal-attribute";
import TextTwig from "./text.twig";

export default {
  title: "Atoms/Text",
  argTypes: {
    content: { control: { type: "text" } },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
    withMargin: { control: { type: "boolean" } },
    center: { control: { type: "boolean" } },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  return TextTwig({ attributes, ...args });
};

export const Text = Template.bind({});
Text.args = {
  content:
    "<h2>Paragraph</h2>" +
    "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Massa sed elementum tempus egestas sed sed risus pretium. Faucibus a pellentesque sit amet. Pellentesque sit amet porttitor eget dolor morbi non. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus.</p>" +
    "<h2>Unordered list</h2>" +
    "<ul><li>Milk</li><li>Cheese<ul><li>Blue cheese</li><li>Feta</li></ul></li></ul>" +
    "<h2>Ordered list</h2>" +
    "<ol><li>Mix flour, baking powder, sugar, and salt.</li><li>In another bowl, mix eggs, milk, and oil.</li><li>Stir both mixtures together.</li><li>Fill muffin tray 3/4 full.</li><li>Bake for 20 minutes.</li></ol>" +
    "<h2>Link</h2>" +
    "<p>You can reach Michael at:</p>" +
    '<ul><li><a href="https://example.com">Website</a></li><li><a href="mailto:m.bluth@example.com">Email</a></li><li><a href="tel:+123456789">Phone</a></li></ul>' +
    "<h2>Strong, Bold</h2>" +
    "<p>Lorem ipsum dolor sit amet, <strong>consectetur</strong> adipiscing elit, sed do eiusmod tempor <b>incididunt</b> ut labore et dolore magna aliqua.</p>" +
    "<h2>Emphasis, Italic</h2>" +
    "<p>Lorem ipsum dolor sit amet, <em>consectetur</em> adipiscing elit, sed do eiusmod tempor <i>incididunt</i> ut labore et dolore magna aliqua.</p>" +
    "<h2>Mark</h2>" +
    '<p>Search results for "salamander":</p>' +
    "<p>Several species of <mark>salamander</mark> inhabit the temperate rainforest of the Pacific Northwest.</p>" +
    "<p>Most <mark>salamander</mark>s are nocturnal, and hunt for insects, worms, and other small creatures.</p>" +
    "<h2>Delete, Insert</h2>" +
    "<blockquote>There is <del>nothing</del> <ins>no code</ins> either good or bad, but <del>thinking</del> <ins>running it</ins> makes it so.</blockquote>" +
    "<h2>Variable</h2>" +
    "<p>The volume of a box is <var>l</var> × <var>w</var> × <var>h</var>, where <var>l</var> represents the length, <var>w</var> the width and <var>h</var> the height of the box.</p>" +
    "<h2>Superscript</h2>" +
    "<p>The <b>Pythagorean theorem</b> is often expressed as the following equation:</p>" +
    "<p><var>a<sup>2</sup></var> + <var>b<sup>2</sup></var> = <var>c<sup>2</sup></var></p>" +
    "<h2>Subscript</h2>" +
    '<p>Almost every developer\'s favorite molecule is C<sub>8</sub>H<sub>10</sub>N<sub>4</sub>O<sub>2</sub>, also known as "caffeine."</p>' +
    "<h2>Code</h2>" +
    "<p>The <code>push()</code> method adds one or more elements to the end of an array and returns the new length of the array.</p>" +
    "<h2>Keyboard Input</h2>" +
    "<p>Please press <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>R</kbd> to re-render an MDN page.</p>" +
    "<h2>Sample Output</h2>" +
    "<p>I was trying to boot my computer, but I got this hilarious message:</p>" +
    "<p><samp>Keyboard not found <br>Press F1 to continue</samp></p>" +
    "<h2>Small</h2>" +
    "<p>MDN Web Docs is a learning platform for Web technologies and the software that powers the Web.</p>" +
    "<p><small>The content is licensed under a Creative Commons Attribution-ShareAlike 2.5 Generic License.</small></p>" +
    "<h2>Abbreviation</h2>" +
    '<p>You can use <abbr title="Cascading Style Sheets">CSS</abbr> to style your <abbr title="HyperText Markup Language">HTML</abbr>.</p>' +
    "<h2>Description List, Term, Detail</h2>" +
    "<p>Cryptids of Cornwall:</p>" +
    "<dl>" +
    "    <dt>Beast of Bodmin</dt>" +
    "    <dd>A large feline inhabiting Bodmin Moor.</dd>" +
    "    <dt>Morgawr</dt>" +
    "    <dd>A sea serpent.</dd>" +
    "    <dt>Owlman</dt>" +
    "    <dd>A giant owl-like creature.</dd>" +
    "</dl>" +
    "<h2>Definition</h2>" +
    '<p>A <dfn id="def-validator">validator</dfn> is a program that checks for syntax errors in code or documents.</p>' +
    "<h2>Quotation</h2>" +
    "<p>When Dave asks HAL to open the pod bay door, HAL answers: <q cite=\"https://www.imdb.com/title/tt0062622/quotes/qt0396921\">I'm sorry, Dave. I'm afraid I can't do that.</q></p>" +
    "<h2>Preformatted</h2>" +
    "<pre>" +
    "L          TE\n" +
    "  A       A\n" +
    "    C    V\n" +
    "      R A\n" +
    "      DOU\n" +
    "      LOU\n" +
    "     REUSE\n" +
    "     QUE TU\n" +
    "     PORTES\n" +
    "   ET QUI T'\n" +
    "   ORNE O CI\n" +
    "    VILISÉ\n" +
    "   OTE-  TU VEUX\n" +
    "    LA    BIEN\n" +
    "   SI      RESPI\n" +
    "           RER       - Apollinaire" +
    "</pre>",
  size: "base",
  colorMode: "auto",
  center: false,
};
