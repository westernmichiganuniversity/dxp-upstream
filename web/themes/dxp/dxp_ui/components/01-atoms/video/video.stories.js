import VideoTwig from "./video.twig";

export default {
  title: "Atoms/Video",
  argTypes: {
    src: { control: { type: "text" } },
    width: { control: { type: "text" } },
    height: { control: { type: "text" } },
    title: { control: { type: "text" } },
    allow: { control: { type: "text" } },
  },
};

const Template = ({ ...args }) => {
  return VideoTwig({ ...args });
};

export const YouTube = Template.bind({});
YouTube.args = {
  src: "https://www.youtube.com/embed/Sao7o8xtTA0",
  width: "560",
  height: "315",
  title: "YouTube video player",
  allow:
    "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
};
YouTube.storyName = "YouTube";

export const Vimeo = Template.bind({});
Vimeo.args = {
  src: "https://player.vimeo.com/video/374753982?h=cc5ca4cf10&amp;app_id=122963",
  width: "426",
  height: "178",
  title: "WMU Montage",
  allow: "autoplay; fullscreen; picture-in-picture",
};
