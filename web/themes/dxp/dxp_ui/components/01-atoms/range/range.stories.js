import DrupalAttribute from "drupal-attribute";
import RangeTwig from "./range.twig";

export default {
  title: "Atoms/Range",
  argTypes: {
    id: { table: { disable: true } },
    name: { table: { disable: true } },
    min: { type: "number" },
    max: { type: "number" },
    step: { type: "number" },
    isDisabled: { control: { type: "boolean" } },
    hasErrors: { control: { type: "boolean" } },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const knownAttributes = ["id", "name", "min", "max", "step"];
  knownAttributes.forEach((attr) => {
    if (args[attr]) {
      attributes.setAttribute(attr, args[attr]);
    }
  });
  return RangeTwig({ attributes, ...args });
};

export const Range = Template.bind({});
Range.args = {
  id: "story",
  name: "story",
  min: 0,
  max: 11,
  step: 1,
  isDisabled: false,
  hasErrors: false,
  colorMode: "auto",
};
