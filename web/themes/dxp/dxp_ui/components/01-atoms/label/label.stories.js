import DrupalAttribute from "drupal-attribute";
import LabelTwig from "./label.twig";

export default {
  title: "Atoms/Label",
  argTypes: {
    for: { table: { disable: true } },
    label: { control: { type: "text" } },
    variant: {
      control: { type: "radio" },
      options: ["before", "after", "invisible"],
    },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    isRequired: { control: { type: "boolean" } },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  attributes.setAttribute("for", args.for);
  return LabelTwig({ attributes, ...args });
};

export const Label = Template.bind({});
Label.args = {
  for: "story",
  label: "Label",
  variant: "before",
  size: "base",
  isRequired: false,
  colorMode: "auto",
};
