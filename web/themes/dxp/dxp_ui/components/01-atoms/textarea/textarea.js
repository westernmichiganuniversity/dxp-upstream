/**
 * @file
 * Behaviors for the text area component.
 */
import characterLimit from "../../../src/modules/characterLimit";

((Drupal, once) => {
  /**
   * Attaches a character counter to a text area.
   *
   * @param {HTMLElement} wrapper
   *   The <textarea>'s wrapper <div> element.
   */
  function attachCharacterCounter(wrapper) {
    const countContainer = document.createElement("div");
    wrapper.before(countContainer);
    const element = wrapper.querySelector("textarea");
    if (element) {
      characterLimit(element, countContainer);
    }
  }

  /**
   * Initialize the text areas.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches behaviors to text areas.
   */
  Drupal.behaviors.textarea = {
    attach(context) {
      once(
        "dxp-character-limit",
        "[data-dxp-character-limit='textarea']",
        context
      ).forEach(attachCharacterCounter);
    },
  };
})(Drupal, once);
