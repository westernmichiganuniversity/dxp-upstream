import DrupalAttribute from "drupal-attribute";
import TextAreaTwig from "./textarea.twig";
import "./textarea";

export default {
  title: "Atoms/Text Area",
  argTypes: {
    id: { table: { disable: true } },
    name: { table: { disable: true } },
    value: { control: { type: "text" } },
    isDisabled: { control: { type: "boolean" } },
    isReadOnly: { control: { type: "boolean" } },
    hasErrors: { control: { type: "boolean" } },
    characterLimit: { control: { type: "number", min: 0 } },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const wrapperAttributes = new DrupalAttribute();
  const attributes = new DrupalAttribute();
  const knownAttributes = ["id", "name", "placeholder", "rows"];
  knownAttributes.forEach((attr) => {
    if (args[attr]) {
      attributes.setAttribute(attr, args[attr]);
    }
  });
  return TextAreaTwig({ wrapperAttributes, attributes, ...args });
};

export const TextArea = Template.bind({});
TextArea.args = {
  id: "story",
  name: "story",
  value: "",
  isDisabled: false,
  isReadOnly: false,
  hasErrors: false,
  characterLimit: 255,
  size: "base",
  colorMode: "auto",
};
