import DrupalAttribute from "drupal-attribute";
import TextFieldTwig from "./textfield.twig";
import "./textfield";

export default {
  title: "Atoms/Text Field",
  argTypes: {
    id: { table: { disable: true } },
    name: { table: { disable: true } },
    type: { table: { disable: true } },
    value: { control: { type: "text" } },
    isDisabled: { control: { type: "boolean" } },
    isReadOnly: { control: { type: "boolean" } },
    hasErrors: { control: { type: "boolean" } },
    characterLimit: { control: { type: "number", min: 0 } },
    hasPrefix: { control: { type: "boolean" } },
    hasSuffix: { control: { type: "boolean" } },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const knownAttributes = [
    "autocomplete",
    "id",
    "max",
    "min",
    "name",
    "pattern",
    "placeholder",
    "step",
    "type",
    "value",
  ];
  knownAttributes.forEach((attr) => {
    if (args[attr]) {
      attributes.setAttribute(attr, args[attr]);
    }
  });
  return TextFieldTwig({ attributes, ...args });
};

export const TextField = Template.bind({});
TextField.args = {
  id: "story",
  name: "story",
  isDisabled: false,
  isReadOnly: false,
  hasErrors: false,
  characterLimit: 20,
  hasPrefix: false,
  hasSuffix: false,
  size: "base",
  colorMode: "auto",
};

export const Date = Template.bind({});
Date.args = {
  ...TextField.args,
  type: "date",
  min: "2021-01-01",
  max: "2021-12-31",
};

export const DatetimeLocal = Template.bind({});
DatetimeLocal.args = {
  ...TextField.args,
  type: "datetime-local",
  min: "2021-01-01T00:00",
  max: "2021-12-31T23:59",
};

export const Email = Template.bind({});
Email.args = {
  ...TextField.args,
  type: "email",
  pattern: ".+@wmich\\.edu",
  placeholder: "your.name@wmich.edu",
};

export const Month = Template.bind({});
Month.args = {
  ...TextField.args,
  type: "month",
  min: "2021-01",
  max: "2021-12",
};

export const Number = Template.bind({});
Number.args = { ...TextField.args, type: "number", min: "10", max: "100" };

export const Password = Template.bind({});
Password.args = {
  ...TextField.args,
  type: "password",
  placeholder: "Password",
};

export const Search = Template.bind({});
Search.args = { ...TextField.args, type: "search" };

export const Tel = Template.bind({});
Tel.args = {
  ...TextField.args,
  type: "tel",
  pattern: "\\([0-9]{3}\\) [0-9]{3}-[0-9]{4}",
  placeholder: "(123) 456-7890",
};

export const Time = Template.bind({});
Time.args = { ...TextField.args, type: "time", min: "09:00", max: "18:00" };

export const Url = Template.bind({});
Url.args = {
  ...TextField.args,
  type: "url",
  pattern: "https://.*",
  placeholder: "https://wmich.edu",
};

export const Week = Template.bind({});
Week.args = {
  ...TextField.args,
  type: "week",
  min: "2021-W01",
  max: "2021-W52",
};
