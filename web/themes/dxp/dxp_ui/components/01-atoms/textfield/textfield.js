/**
 * @file
 * Behaviors for the text field component.
 */
import characterLimit from "../../../src/modules/characterLimit";

((Drupal, once) => {
  /**
   * Attaches a character counter to a text field.
   *
   * @param {HTMLElement} element
   *   The input element.
   */
  function attachCharacterCounter(element) {
    const countContainer = document.createElement("div");
    element.before(countContainer);
    characterLimit(element, countContainer);
  }

  /**
   * Initialize the text fields.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches behaviors to text fields.
   */
  Drupal.behaviors.textfield = {
    attach(context) {
      once(
        "dxp-character-limit",
        "[data-dxp-character-limit='textfield']",
        context
      ).forEach(attachCharacterCounter);
    },
  };
})(Drupal, once);
