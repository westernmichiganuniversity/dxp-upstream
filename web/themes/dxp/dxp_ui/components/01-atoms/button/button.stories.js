import DrupalAttribute from "drupal-attribute";
import ButtonTwig from "./button.twig";

export default {
  title: "Atoms/Button",
  argTypes: {
    type: { control: { type: "radio" }, options: ["button", "submit", "link"] },
    href: { control: { type: "text" } },
    id: { table: { disable: true } },
    name: { table: { disable: true } },
    value: { table: { disable: true } },
    label: { control: { type: "text" } },
    variant: {
      control: { type: "radio" },
      options: ["primary", "secondary", "danger"],
    },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    width: {
      control: { type: "radio" },
      options: ["auto", "full", "responsive"],
    },
    isQuiet: { control: { type: "boolean" } },
    isDisabled: { control: { type: "boolean" } },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
    isPrefix: { control: { type: "boolean" } },
    isSuffix: { control: { type: "boolean" } },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  let knownAttributes = ["id", "name", "type", "value"];
  if (args.type === "link") {
    knownAttributes = ["id", "href"];
  }
  knownAttributes.forEach((attr) => {
    if (args[attr]) {
      attributes.setAttribute(attr, args[attr]);
    }
  });
  return ButtonTwig({ attributes, ...args });
};

export const Button = Template.bind({});
Button.args = {
  type: "button",
  href: "",
  id: "story-button",
  name: "op",
  value: "Button",
  label: "Button",
  variant: "secondary",
  size: "base",
  width: "auto",
  isQuiet: false,
  isDisabled: false,
  colorMode: "auto",
  isPrefix: false,
  isSuffix: false,
};

export const Primary = Template.bind({});
Primary.args = { ...Button.args, label: "Save", variant: "primary" };

export const Secondary = Template.bind({});
Secondary.args = { ...Button.args, label: "Preview", variant: "secondary" };

export const Danger = Template.bind({});
Danger.args = { ...Button.args, label: "Delete", variant: "danger" };

export const FullWidth = Template.bind({});
FullWidth.args = { ...Button.args, width: "full" };
