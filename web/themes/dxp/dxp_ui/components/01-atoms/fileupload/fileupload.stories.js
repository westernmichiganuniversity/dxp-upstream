import DrupalAttribute from "drupal-attribute";
import FileUploadTwig from "./fileupload.twig";

export default {
  title: "Atoms/File Upload",
  argTypes: {
    id: { table: { disable: true } },
    name: { table: { disable: true } },
    accept: { table: { disable: true } },
    isDisabled: { control: { type: "boolean" } },
    hasErrors: { control: { type: "boolean" } },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const knownAttributes = ["id", "name", "accept"];
  knownAttributes.forEach((attr) => {
    if (args[attr]) {
      attributes.setAttribute(attr, args[attr]);
    }
  });
  return FileUploadTwig({ attributes, ...args });
};

export const FileUpload = Template.bind({});
FileUpload.args = {
  id: "story",
  name: "story",
  accept: "image/png, image/jpeg",
  isDisabled: false,
  hasErrors: false,
  size: "base",
  colorMode: "auto",
};
