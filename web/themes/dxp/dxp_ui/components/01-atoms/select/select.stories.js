import DrupalAttribute from "drupal-attribute";
import SelectTwig from "./select.twig";

export default {
  title: "Atoms/Select",
  argTypes: {
    id: { table: { disable: true } },
    name: { table: { disable: true } },
    options: { control: { type: "object" } },
    isDisabled: { control: { type: "boolean" } },
    hasErrors: { control: { type: "boolean" } },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const knownAttributes = ["autocomplete", "id", "name"];
  knownAttributes.forEach((attr) => {
    if (args[attr]) {
      attributes.setAttribute(attr, args[attr]);
    }
  });
  return SelectTwig({ attributes, ...args });
};

export const Select = Template.bind({});
Select.args = {
  id: "story",
  name: "story",
  options: [
    { type: "option", value: "US", selected: true, label: "United States" },
    { type: "option", value: "CA", selected: false, label: "Canada" },
    { type: "option", value: "MX", selected: false, label: "Mexico" },
  ],
  isDisabled: false,
  hasErrors: false,
  size: "base",
  colorMode: "auto",
};

export const OptionGroup = Template.bind({});
OptionGroup.args = {
  ...Select.args,
  options: [
    {
      type: "optgroup",
      label: "North America",
      options: [
        { type: "option", value: "US", selected: true, label: "United States" },
        { type: "option", value: "CA", selected: false, label: "Canada" },
        { type: "option", value: "MX", selected: false, label: "Mexico" },
      ],
    },
  ],
};
