import DrupalAttribute from "drupal-attribute";
import LayeredIconTwig from "./layeredicon.twig";

export default {
  title: "Molecules/Layered Icon",
  argTypes: {
    icons: { control: { type: "object" } },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl", "2xl", "3xl", "4xl", "5xl"],
    },
    counter: { type: "text" },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  return LayeredIconTwig({ ...args, attributes });
};

export const LayeredIcon = Template.bind({});
LayeredIcon.args = {
  size: "5xl",
  icons: [
    {
      prefix: "fas",
      icon: "fa-envelope",
    },
  ],
  counter: "1,538",
};
