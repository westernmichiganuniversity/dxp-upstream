import DrupalAttribute from "drupal-attribute";
import AlertTwig from "./alert.twig";

export default {
  title: "Molecules/Alert",
  argTypes: {
    label: { control: "text" },
    level: {
      control: { type: "select" },
      options: [1, 2, 3, 4, 5, 6],
    },
    content: { control: "text" },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const labelAttributes = new DrupalAttribute();
  return AlertTwig({ attributes, labelAttributes, ...args });
};

export const Alert = Template.bind({});
Alert.args = {
  label: "Kaiju sighting",
  content:
    "Seek shelter immediately and await further instrutions. <strong>Monday, Dec. 13, 2021</strong>.",
};
