import DrupalAttribute from "drupal-attribute";
import MediaTwig from "./media.twig";
import { Image } from "../../01-atoms/image/image.stories";

export default {
  title: "Molecules/Media",
  argTypes: {
    size: {
      control: { type: "select" },
      options: [
        "full",
        "prose",
        "7xl",
        "6xl",
        "5xl",
        "4xl",
        "3xl",
        "2xl",
        "xl",
        "lg",
        "md",
        "sm",
        "xs",
      ],
    },
    float: {
      control: { type: "select" },
      options: ["none", "left", "right"],
    },
    label: { control: "text" },
    caption: { control: "text" },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  return MediaTwig({ attributes, ...args });
};

export const Media = Template.bind({});
Media.args = {
  media: Image({ ...Image.args }),
  label: "An image with a caption",
  caption:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam purus nibh, blandit ac dolor non, efficitur eleifend ex. Integer enim augue, convallis at varius nec, faucibus vel enim.",
};
