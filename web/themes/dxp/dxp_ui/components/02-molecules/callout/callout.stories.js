import DrupalAttribute from "drupal-attribute";
import CalloutTwig from "./callout.twig";

export default {
  title: "Molecules/Callout",
  argTypes: {
    heading: { control: "text" },
    headingLevel: {
      control: { type: "select" },
      options: [1, 2, 3, 4, 5, 6],
    },
    headingWeight: {
      control: { type: "select" },
      options: ["light", "medium", "heavy"],
    },
    text: { control: "text" },
    links: { control: { type: "object" } },
    linkDisplay: {
      control: { type: "select" },
      options: [
        "cta",
        "primary_secondary",
        "cta_alt",
        "secondaries_only",
        "list",
      ],
    },
    align: {
      control: { type: "radio" },
      options: ["left", "center"],
    },
    numOfLinksShown: {
      control: {
        type: "number",
        min: 1,
        max: 9,
        step: 1,
      },
    },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
    withMargin: { control: { type: "boolean" } },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const headingAttributes = new DrupalAttribute();
  const textAttributes = new DrupalAttribute();

  // Allow Storybook to display an arbitrary number of links in the preview.
  const links = [];
  for (let i = 0; i < args.numOfLinksShown; i++) {
    if (i in args.links) {
      links.push({
        attributes: new DrupalAttribute(),
        label: args.links[i].label,
        href: args.links[i].href,
      });
    } else {
      links.push({
        attributes: new DrupalAttribute(),
        label: `Example link ${i + 1}`,
        href: "https://wmich.edu",
      });
    }
  }

  return CalloutTwig({
    ...args,
    attributes,
    headingAttributes,
    textAttributes,
    links,
  });
};

export const Callout = Template.bind({});
Callout.args = {
  heading: "Become a Bronco",
  headingLevel: 2,
  headingWeight: "medium",
  text: "Western will help you pursue your purpose in an environment where you can thrive academically, emotionally and physically. We focus on your individual well-being so you can better learn the critical skills needed to build a meaningful career and craft a life well-lived.",
  links: [
    {
      label: "Apply as a freshman",
      href: "https://www.commonapp.org/explore/western-michigan-university",
    },
    {
      label: "View app deadlines",
      href: "https://wmich.edu/apply/deadlines",
    },
  ],
  linkDisplay: "cta_alt",
  align: "left",
  numOfLinksShown: 2,
  size: "base",
  colorMode: "auto",
  withMargin: false,
};

export const Primary = Template.bind({});
Primary.args = {
  ...Callout.args,
  linkDisplay: "cta",
};

export const PrimaryAndSecondary = Template.bind({});
PrimaryAndSecondary.args = {
  ...Callout.args,
  linkDisplay: "primary_secondary",
};

export const SecondariesOnly = Template.bind({});
SecondariesOnly.args = {
  ...Callout.args,
  linkDisplay: "secondaries_only",
};

export const List = Template.bind({});
List.args = {
  ...Callout.args,
  linkDisplay: "list",
};

export const Centered = Template.bind({});
Centered.args = {
  ...Callout.args,
  linkDisplay: "cta",
  numOfLinksShown: 6,
  align: "center",
};
