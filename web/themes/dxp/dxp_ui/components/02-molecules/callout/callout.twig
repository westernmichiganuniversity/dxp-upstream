{#
/**
 * @file
 * Callout component.
 *
 * Available variables:
 * - attributes: A list of HTML attributes for the wrapper element.
 * - heading: The heading text.
 * - headingAttributes: A list of HTML attributes for the callout heading.
 * - headingLevel: An integer 1..6 indicating which <h#> to use. Default is 2.
 * - headingWeight: (light|medium|heavy) The visual weight of the heading.
 * - text: The text content.
 * - textAttributes: A list of HTML attributes for the callout text.
 * - attribution: Attribution of the content.
 * - links: (required) An array of links. Each link consists of the following:
 *   - attributes: A list of HTML attributes for the link.
 *   - label: The link text.
 *   - href: The link URL.
 * - linkDisplay: Controls the link rendering as follows:
 *   - 'cta': Render all links as Primary Buttons.
 *   - 'primary_secondary': Render the first link as a Primary Button, the rest
 *     as Secondary Buttons.
 *   - 'cta_alt': Render the first link as a Primary Button, the rest as quiet
 *     Secondary Buttons.
 *   - 'secondaries_only': Render all the buttons as Secondary Buttons.
 *   - 'list': Render the links as an unordered list of Links.
 * - align: Controls alignment and button tiling as follows:
 *   - 'left': Heading, text and links/buttons are left-aligned and buttons are
 *     sized to their label. This is the default.
 *   - 'center': Heading, text and links/buttons are centered and buttons are
 *     displayed as a grid with equalized widths.
 * - size: (sm|base|lg|xl) Controls the size of the callout. Default is 'base'.
 * - colorMode: (auto|light|dark) Modifies the color rendering as follows:
 *   - 'auto': the link is against the site's default background and thus
 *     should adjust its color as needed for light/dark mode.
 *   - 'light': the link is against a light background color and thus should
 *     always use its light mode color.
 *   - 'dark': the link is against a dark background color and thus should
 *     always use its dark mode color.
 * - withMargin: Boolean indicating whether to set the bottom margin.
 *
 * @see paragraph--callout.html.twig
 */
#}
{% set headingLevel = headingLevel ?? 2 %}
{% set headingWeight = headingWeight ?? 'medium' %}
{% set linkDisplay = linkDisplay ?? 'cta_alt' %}
{% set align = align ?? 'left' %}
{% set size = size ?? 'base' %}
{% set colorMode = colorMode ?? 'auto' %}
{%
  set classes = [
    heading is empty and text is empty ? 'tw-w-full' : 'tw-w-fit',
    heading is empty and text is empty ? 'tw-max-w-7xl' : '',
    'tw-mx-auto',
  ]
%}
{% if align == 'center' %}
  {% set headingAttributes = headingAttributes.addClass('sm:tw-text-center') %}
  {%
    set textAttributes = textAttributes.addClass([
      'sm:tw-text-center',
      'sm:tw-mx-auto',
    ])
  %}
{% endif %}
{% if withMargin -%}
  {% set classes = classes|merge(['tw-mb-8']) %}
{%- endif %}
{% if size == 'sm' -%}
  {% set classes = classes|merge(['tw-space-y-4']) %}
  {% set headingAttributes = headingAttributes.addClass('tw--mb-2') %}
{% elseif size == 'lg' -%}
  {% set classes = classes|merge(['tw-space-y-6']) %}
  {% set headingAttributes = headingAttributes.addClass('tw--mb-2') %}
{% elseif size == 'xl' -%}
  {%
    set classes = classes|merge([
      heading is empty ? 'tw-space-y-7' : 'tw-space-y-11',
    ])
  %}
  {% set headingAttributes = headingAttributes.addClass('tw--mb-3') %}
{% else -%}
  {# size == 'base' #}
  {% set classes = classes|merge(['tw-space-y-5']) %}
  {% set headingAttributes = headingAttributes.addClass('tw--mb-2') %}
{%- endif %}
{%
  set headingSizingMap = {
    'sm': 'md',
    'base': 'lg',
    'lg': 'xl',
    'xl': '2xl',
  }
%}
{%
  set headingVars = {
    'attributes': headingAttributes,
    'level': headingLevel,
    'size': headingSizingMap[size],
    'weight': headingWeight,
    'label': heading,
    'colorMode': colorMode,
    'withMargin': false,
  }
%}
{%
  set textVars = {
    'attributes': textAttributes,
    'content': text,
    'size': size,
    'colorMode': colorMode,
    'withMargin': false,
    'center': false,
  }
%}
<div{{ attributes.addClass(classes) }}>
  {% if heading -%}
    {% include '@atoms/heading/heading.twig' with headingVars only %}
  {%- endif %}
  {% if linkDisplay == 'list' -%}
    {# Links displayed as a list are rendered as part of the text. #}
    {% set textVars = textVars|merge({'links': links}) %}
    {% embed '@atoms/text/text.twig' with textVars only %}
      {% block text_content %}
        {{ content }}
        <ul>
          {% for item in links -%}
            <li{{ item.attributes }}><a href="{{ item.href }}">{{ item.label }}</a></li>
          {%- endfor %}
        </ul>
      {% endblock %}
    {% endembed %}
  {% else -%}
    {# Links displayed as buttons are rendered separately, after the text. #}
    {% if text -%}
      {% include '@atoms/text/text.twig' with textVars only %}
    {%- endif %}
    {% if attribution -%}
      {%
        set attributionClasses = [
          'tw-italic',
          'tw-text-sm',
          'md:tw-text-base',
          '!tw-mt-0',
          colorMode == 'auto' ? 'dark:tw-text-gray-50' : '',
          colorMode == 'dark' ? 'tw-text-gray-50' : 'tw-text-gray-600',
        ]
      %}
      <div class="{{ attributionClasses|join(' ') }}">&mdash;{{ attribution }}</div>
    {%- endif %}
    {%
      set contentClasses = [
        'tw-flex-1',
        'tw-flex',
        'tw-flex-row',
        'tw-flex-wrap',
        heading is empty and text is empty ? 'tw-gap-8' : 'tw-gap-4',
        'tw-items-end',
        'tw-content-end',
        heading is not empty or text is not empty ? 'tw-max-w-prose' : '',
      ]
    %}
    {% set linkWidth = 'responsive' %}
    {% if align == 'center' -%}
      {% set contentClasses = contentClasses|merge(['tw-mx-auto']) %}
      {% if links|length > 1 -%}
        {# When there is more than 1 centered button, equalize their widths. #}
        {% set linkWidth = 'full' %}
        {%
          set contentClasses = contentClasses|merge([
            'sm:tw-grid',
            'sm:tw-items-stretch',
            'sm:tw-justify-items-center',
            links|length is odd ? 'sm:tw-grid-cols-3' : 'sm:tw-grid-cols-2',
          ])
        %}
      {% else -%}
        {% set contentClasses = contentClasses|merge(['sm:tw-justify-center']) %}
      {%- endif %}
    {%- endif %}
    <div class="{{ contentClasses|join(' ') }}">
      {% for item in links -%}
        {%
          set linkVars = {
            'attributes': item.attributes,
            'type': 'link',
            'href': item.href,
            'label': item.label,
            'size': size,
            'width': linkWidth,
            'colorMode': colorMode,
          }
        %}
        {% if linkDisplay == 'cta' %}
          {%
            set linkVars = linkVars|merge({
              'variant': 'primary',
            })
          %}
        {% elseif linkDisplay == 'primary_secondary' %}
          {%
            set linkVars = linkVars|merge({
              'variant': loop.first ? 'primary' : 'secondary',
            })
          %}
        {% elseif linkDisplay == 'cta_alt' %}
          {%
            set linkVars = linkVars|merge({
              'variant': loop.first ? 'primary' : 'secondary',
              'isQuiet': (not loop.first),
            })
          %}
        {% else %}
          {# linkDisplay == 'secondaries_only' #}
          {%
            set linkVars = linkVars|merge({
              'variant': 'secondary',
            })
          %}
        {% endif %}
        {% include '@atoms/button/button.twig' with linkVars only %}
      {%- endfor %}
    </div>
  {%- endif %}
</div>
