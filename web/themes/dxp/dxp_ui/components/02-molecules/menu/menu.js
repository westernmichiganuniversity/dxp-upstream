/**
 * @file
 * Behaviors for the menu component.
 */
((Drupal, once) => {
  /**
   * Click menu behaviors.
   *
   * @param {HTMLElement} menu
   *   The menu element.
   *
   * @see https://css-tricks.com/in-praise-of-the-unambiguous-click-menu/
   * @see https://www.w3.org/TR/wai-aria-practices-1.1/examples/disclosure/disclosure-navigation.html
   * @see https://www.lullabot.com/articles/accessible-navigation-drupal-cores-menu-system
   */
  const DxpClickMenu = function attachClickMenu(menu) {
    // Properties.
    const container = menu.parentElement;
    const currentOpen = [];

    /**
     * Converts a button-type menu item to a <button>.
     *
     * @param {HTMLElement} element
     *   The element to convert to a button.
     *
     * @return {HTMLElement}
     *   The button.
     */
    function convertItemToButton(element) {
      const button = document.createElement("button");
      button.innerHTML = element.outerHTML;
      button.classList.add(
        "tw-flex-initial",
        "tw-basis-0",
        "focus-visible:tw-outline-none",
        "focus-visible:tw-ring-2",
        "focus-visible:tw-ring-blue-500",
        "focus-visible:tw-ring-offset-4",
        "focus-visible:tw-ring-offset-white",
        "dark:focus-visible:tw-ring-offset-gray-800"
      );
      element.parentNode.replaceChild(button, element);
      return button;
    }

    /**
     * Checks whether a button is a child of an open menu.
     *
     * @param {HTMLElement} button
     *   A button.
     *
     * @return {boolean}
     *   TRUE if the button is in an open menu.
     */
    function isChildOfOpen(button) {
      let result = false;
      currentOpen.forEach((open) => {
        // Get the submenu controlled by the open item.
        const submenu = document.getElementById(
          open.getAttribute("aria-controls")
        );
        if (submenu.contains(button)) {
          result = true;
        }
      });
      return result;
    }

    /**
     * Toggles a button's submenu.
     *
     * @param {HTMLElement} button
     *   The button that controls the submenu.
     */
    function toggleSubmenu(button) {
      const submenu = document.getElementById(
        button.getAttribute("aria-controls")
      );
      if (button.getAttribute("aria-expanded") === "true") {
        button.setAttribute("aria-expanded", "false");
        submenu.setAttribute("aria-hidden", "true");
        const index = currentOpen.indexOf(button);
        if (index > -1) {
          currentOpen.splice(index, 1);
        }
      } else {
        button.setAttribute("aria-expanded", "true");
        submenu.setAttribute("aria-hidden", "false");
        currentOpen.push(button);
      }
      if (currentOpen.length > 0) {
        const firstSubmenu = document.getElementById(
          currentOpen[0].getAttribute("aria-controls")
        );
        firstSubmenu.classList.add("tw-right-0");
        if (firstSubmenu.offsetLeft >= currentOpen[0].offsetLeft) {
          firstSubmenu.classList.remove("tw-right-0");
        }
      }
    }

    /**
     * Closes all open submenus.
     */
    function closeCurrentOpen() {
      const cloneCurrentOpen = [...currentOpen];
      cloneCurrentOpen.forEach(toggleSubmenu);
    }

    /**
     * Toggles menus in response to a click event.
     *
     * @param {Event} e
     *   The click event.
     */
    function toggleOnMenuClick(e) {
      const button = e.currentTarget;
      const index = currentOpen.indexOf(button);
      if (index === -1 && !isChildOfOpen(button)) {
        closeCurrentOpen();
      }
      toggleSubmenu(button);
    }

    /**
     * Initializes the click menu.
     */
    this.init = function clickMenuInit() {
      // Find all button-type menu items.
      menu
        .querySelectorAll("[data-dxp-menu-item-type='button']")
        .forEach((element) => {
          const submenuId = element.getAttribute("data-dxp-menu-item-controls");
          if (submenuId) {
            const button = convertItemToButton(element);
            const submenu = document.getElementById(submenuId);

            // Set ARIA attributes so that screen readers understand menu state.
            button.setAttribute("aria-controls", submenuId);
            button.setAttribute("aria-expanded", "false");
            submenu.setAttribute("aria-hidden", "true");

            // Add event listeners to control the submenu.
            button.addEventListener("click", toggleOnMenuClick);
          }
        });

      // Close all submenus if the user presses the Escape key.
      menu.addEventListener("keyup", (e) => {
        if (e.code === "Escape") {
          // @todo This could be refined to only close the current submenu for keyboard navigation users moving through nested submenus.
          closeCurrentOpen();
        }
      });

      // Close all submenus if the user clicks outside the menu.
      document.addEventListener("click", (e) => {
        if (container.id && !e.target.closest(`#${container.id}`)) {
          closeCurrentOpen();
        }
      });
    };
  };

  /**
   * Initialize the menu component.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the click menu behavior to .dxp-ui-menu elements.
   */
  Drupal.behaviors.dxpUiMenu = {
    attach(context) {
      once("dxp-click-menu", ".dxp-ui-menu", context).forEach((menu) => {
        const clickMenu = new DxpClickMenu(menu);
        clickMenu.init();
      });
    },
  };
})(Drupal, once);
