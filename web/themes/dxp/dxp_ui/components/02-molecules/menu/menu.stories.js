import DrupalAttribute from "drupal-attribute";
import MenuTwig from "./menu.twig";
import "./menu";

export default {
  title: "Molecules/Menu",
  decorators: [(story) => `<nav id="story-menu">${story()}</nav>`],
  argTypes: {
    variant: {
      control: { type: "radio" },
      options: ["main", "header", "footer", "sidebar"],
    },
    items: { control: { type: "object" } },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const prepareItems = (items) => {
  const prepared = [];
  items.forEach((item) => {
    if (item.href && item.href === "<button>") {
      item.type = "button";
    } else if (item.href) {
      item.type = "link";
    } else {
      item.type = "nolink";
    }
    if (item.children) {
      const childId = `submenu-${item.label}`
        .split(" ")
        .join("-")
        .toLowerCase();
      item.children = {
        // See https://github.com/ericmorand/drupal-attribute/issues/9.
        // We have to provide id as a separate value (rather than just setting
        // it in attributes) because Twig.js can't access attributes.id directly
        // the way actual Twig does. Thus, we use item.children.id in menuMacro
        // (rather than item.children.attributes.id) so that it works for both
        // Storybook and Drupal.
        id: childId,
        attributes: new DrupalAttribute([["id", childId]]),
        items: prepareItems(item.children),
      };
    }
    prepared.push({
      attributes: new DrupalAttribute(),
      ...item,
    });
  });
  return prepared;
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const items = prepareItems(args.items);
  return MenuTwig({ ...args, attributes, items });
};

export const Main = Template.bind({});
Main.args = {
  variant: "main",
  items: [
    {
      label: "About",
      href: "<button>",
      inActiveTrail: true,
      children: [
        {
          label: "About WMU",
          href: "https://wmich.edu/about",
        },
        {
          label: "Campuses and Facilities",
          href: "https://wmich.edu/about/campuses",
        },
        {
          label: "Fast Facts",
          href: "https://wmich.edu/about/facts",
        },
        {
          label: "Bronco Impact",
          href: "https://wmich.edu/about/impact",
        },
        {
          label: "Discover Kalamazoo",
          href: "https://wmich.edu/about/kalamazoo",
        },
        {
          label: "Mission and Vision",
          href: "https://wmich.edu/about/mission",
        },
        {
          label: "Our Values",
          href: "<button>",
          inActiveTrail: true,
          children: [
            {
              label: "Building the Future",
              href: "https://wmich.edu/about/hilltop",
            },
            {
              label: "Diversity and Inclusion",
              href: "https://wmich.edu/about/diversity",
              inActiveTrail: true,
            },
            {
              label: "Sustainability",
              href: "https://wmich.edu/about/sustainability",
            },
          ],
        },
      ],
    },
    {
      label: "Academics",
      href: "<button>",
      children: [
        {
          label: "Why Choose WMU?",
          href: "https://wmich.edu/academics",
        },
        {
          label: "Undergraduate Programs",
          href: "https://wmich.edu/academics/undergraduate",
        },
        {
          label: "Graduate Programs",
          href: "https://wmich.edu/academics/graduate",
        },
        {
          label: "Colleges and Departments",
          href: "https://wmich.edu/academics/colleges-departments",
        },
      ],
    },
    {
      label: "Admissions",
      href: "<button>",
      children: [
        {
          label: "How to Apply",
          href: "https://wmich.edu/admissions",
        },
        {
          label: "First-Year Students",
          href: "https://wmich.edu/admissions/freshmen",
        },
        {
          label: "Transfer Students",
          href: "https://wmich.edu/admissions/transfer",
        },
        {
          label: "International Students",
          href: "https://wmich.edu/admissions/international",
        },
        {
          label: "Graduate Students",
          href: "https://wmich.edu/admissions/graduate",
        },
      ],
    },
    {
      label: "Financial Aid",
      href: "https://wmich.edu/finaid",
    },
    {
      label: "Campus Life",
      href: "https://wmich.edu/students",
    },
    {
      label: "Athletics",
      href: "https://wmubroncos.com/",
    },
    {
      label: "Research",
      href: "https://wmich.edu/research",
    },
  ],
};

export const Header = Template.bind({});
Header.args = {
  variant: "header",
  items: [
    {
      label: "Apply",
      href: "https://wmich.edu/apply",
      inActiveTrail: true,
    },
    {
      label: "Give",
      href: "https://secure.wmualumni.org/s/give",
    },
    {
      label: "Visit",
      href: "https://wmich.edu/visit",
    },
  ],
};

export const Footer = Template.bind({});
Footer.args = {
  variant: "footer",
  items: [
    {
      label: "Audiences",
      href: "",
      inActiveTrail: true,
      children: [
        {
          label: "Alumni and Donors",
          href: "https://wmualumni.org/",
        },
        {
          label: "Businesses",
          href: "https://wmich.edu/businessconnection",
          inActiveTrail: true,
        },
        {
          label: "Veterans",
          href: "https://wmich.edu/military",
        },
      ],
    },
    {
      label: "Locations",
      href: "",
      children: [
        {
          label: "Kalamazoo",
          href: "https://wmich.edu/about/kalamazoo",
        },
        {
          label: "Regional Locations",
          href: "https://wmich.edu/extended",
        },
        {
          label: "Online Education",
          href: "https://wmich.edu/online",
        },
        {
          label: "Stryker School of Medicine",
          href: "https://med.wmich.edu/",
        },
      ],
    },
    {
      label: "Services",
      href: "",
      children: [
        {
          label: "Campus Safety Information and Resources",
          href: "https://wmich.edu/campus-safety",
        },
        {
          label: "Careers and Internships",
          href: "https://wmich.edu/career",
        },
        {
          label: "Human Resources",
          href: "https://wmich.edu/hr",
        },
        {
          label: "Maps",
          href: "https://wmich.edu/maps",
        },
        {
          label: "Parking",
          href: "https://wmich.edu/parking",
        },
      ],
    },
  ],
};

export const Sidebar = Template.bind({});
Sidebar.args = {
  variant: "sidebar",
  items: [
    {
      label: "About University Libraries",
      href: "https://library.wmich.edu/about",
    },
    {
      label: "Departments",
      href: "",
      children: [
        {
          label: "Organizational Chart",
          href: "https://library.wmich.edu/about/departments",
        },
        {
          label: "Office of the Dean",
          href: "https://library.wmich.edu/about/departments/dean",
        },
      ],
    },
    {
      label: "Employment",
      href: "https://library.wmich.edu/about/employment",
    },
    {
      label: "Giving",
      href: "https://library.wmich.edu/about/giving",
    },
    {
      label: "Diversity, Equity and Inclusion",
      href: "https://library.wmich.edu/about/dei",
    },
    {
      label: "Facts",
      href: "",
      inActiveTrail: true,
      children: [
        {
          label: "Library Facts 2019–20",
          href: "https://library.wmich.edu/about/facts",
        },
        {
          label: "Annual Report",
          href: "https://library.wmich.edu/about/facts/report",
          inActiveTrail: true,
        },
      ],
    },
    {
      label: "Policies",
      href: "https://library.wmich.edu/about/policies",
    },
  ],
};
