import DrupalAttribute from "drupal-attribute";
import CardTwig from "./card.twig";
import { Icon } from "../../01-atoms/icon/icon.stories";
import {
  ArtDirection,
  Square,
  Image,
} from "../../01-atoms/image/image.stories";
import { YouTube } from "../../01-atoms/video/video.stories";

export default {
  title: "Molecules/Card",
  argTypes: {
    mediaAspect: {
      control: { type: "select" },
      options: ["2:3", "1:1", "4:3", "3:2", "16:9", "auto"],
    },
    label: { control: "text" },
    level: {
      control: { type: "select" },
      options: [1, 2, 3, 4, 5, 6],
    },
    detail: { control: "text" },
    content: { control: "text" },
    link1Label: { control: "text" },
    link1Href: { control: "text" },
    link2Label: { control: "text" },
    link2Href: { control: "text" },
    size: {
      control: { type: "radio" },
      options: ["sm", "md", "lg", "full"],
    },
    variant: {
      control: { type: "radio" },
      options: ["horizontal", "vertical"],
    },
    withActionLinks: { control: "boolean" },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const labelAttributes = new DrupalAttribute();
  const contentAttributes = new DrupalAttribute();
  const links = [];
  if (args.link1Href) {
    links.push({
      attributes: new DrupalAttribute(),
      href: args.link1Href,
      label: args.link1Label,
    });
  }
  if (args.link2Href) {
    links.push({
      attributes: new DrupalAttribute(),
      href: args.link2Href,
      label: args.link2Label,
    });
  }
  return CardTwig({
    ...args,
    attributes,
    labelAttributes,
    contentAttributes,
    links,
  });
};

export const Card = Template.bind({});
Card.args = {
  media: Image({ ...Image.args }),
  mediaAspect: "3:2",
  label: "WMU launches new effort to advance climate commitment",
  level: 2,
  detail: "October 14, 2019",
  content:
    "Western Michigan University is exploring ways to advance its commitment to reducing its carbon footprint, as scientists around the world sound the alarm about a growing climate crisis.",
  link1Href: "https://wmich.edu/news/2019/10/55956",
  size: "md",
  variant: "vertical",
  withActionLinks: false,
  colorMode: "auto",
};

export const WithActionLinks = Template.bind({});
WithActionLinks.args = {
  ...Card.args,
  label: "Become a Bronco",
  detail: null,
  content:
    "We prepare students for a life well-lived so they can pursue their purpose and make a meaningful impact on society.",
  link1Label: "Apply now",
  link1Href: "https://wmich.edu/apply",
  link2Label: "Schedule a tour",
  link2Href: "https://wmich.edu/visit",
  withActionLinks: true,
};

export const Horizontal = Template.bind({});
Horizontal.args = {
  ...WithActionLinks.args,
  variant: "horizontal",
  size: "lg",
};

export const WithVideo = Template.bind({});
WithVideo.args = {
  ...WithActionLinks.args,
  media: YouTube({ ...YouTube.args }),
  mediaAspect: "16:9",
  size: "md",
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  ...Card.args,
  media: Icon({ ...Icon.args, icon: "fa-download", size: "5xl" }),
  mediaAspect: "1:1",
  label: "Visual Identity Guide",
  detail: "PDF, 4.2 MB",
  content: null,
  link1Href:
    "https://wmich.edu/sites/default/files/attachments/u171/2021/VI%20Guide%20Final%203-2-2021.pdf",
  size: "md",
  variant: "horizontal",
};

export const WithArtDirection = Template.bind({});
WithArtDirection.args = {
  ...Card.args,
  media: ArtDirection({ ...ArtDirection.args }),
  mediaAspect: "auto",
  label: "Oh, big stretch!",
  detail: "Google Chrome &lt;picture&gt; element demo",
  link1Href: "https://googlechrome.github.io/samples/picture-element/",
  content: null,
  size: "lg",
};

export const NoMedia = Template.bind({});
NoMedia.args = {
  ...Card.args,
  media: null,
  label: "Career advising",
  detail: null,
  content:
    "At WMU, we want to help you pursue what matters most to you from day one.",
  link1Href: "https://wmich.edu/career",
  size: "sm",
};

export const NoLinksVertical = Template.bind({});
NoLinksVertical.args = {
  media: Square({ ...Square.args }),
  mediaAspect: "1:1",
  content: "Top 11 percent of all universities in the country",
  detail: "U.S. News & World Report",
  size: "sm",
  variant: "vertical",
};

export const NoLinksHorizontal = Template.bind({});
NoLinksHorizontal.args = {
  ...NoLinksVertical.args,
  size: "md",
  variant: "horizontal",
};
