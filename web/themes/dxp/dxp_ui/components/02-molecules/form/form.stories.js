import DrupalAttribute from "drupal-attribute";
import FormTwig from "./form.twig";
import { Actions, Container } from "./container/container.stories";
import { Fieldset, WithDescription } from "./fieldset/fieldset.stories";

export default {
  title: "Molecules/Form",
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const children = args.children.join("");
  return FormTwig({ attributes, children });
};

export const Form = Template.bind({});
Form.args = {
  children: [
    Container({
      ...Container.args,
      children: [
        Fieldset({ ...Fieldset.args }),
        WithDescription({ ...WithDescription.args }),
      ],
    }),
    Actions({
      ...Actions.args,
    }),
  ],
};
