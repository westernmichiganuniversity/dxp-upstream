import DrupalAttribute from "drupal-attribute";
import FieldsetTwig from "./fieldset.twig";
import { Checkboxes, Radios } from "../container/container.stories";

export default {
  title: "Molecules/Form/Fieldset",
  argTypes: {
    legend: { control: { type: "text" } },
    description: { control: { type: "text" } },
    required: { control: { type: "boolean" } },
    prefix: { control: { type: "text" } },
    suffix: { control: { type: "text" } },
    errors: { control: { type: "text" } },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const legend = {
    title: args.legend,
    attributes: new DrupalAttribute(),
  };
  const description = {
    content: args.description,
    attributes: new DrupalAttribute(),
  };
  const children = args.children.join("");
  return FieldsetTwig({
    attributes,
    errors: args.errors,
    required: args.required,
    legend,
    description,
    children,
    prefix: args.prefix,
    suffix: args.suffix,
  });
};

export const Fieldset = Template.bind({});
Fieldset.args = {
  legend: "By Email",
  description: "",
  required: false,
  prefix: "",
  suffix: "",
  errors: "",
  children: [Checkboxes({ ...Checkboxes.args, hasParent: true })],
};

export const WithDescription = Template.bind({});
WithDescription.args = {
  ...Fieldset.args,
  legend: "Push Notifications",
  description: "These are delivered to the mobile app.",
  children: [Radios({ ...Radios.args, hasParent: true })],
};

export const PrefixAndSuffix = Template.bind({});
PrefixAndSuffix.args = {
  ...Fieldset.args,
  prefix: "Prefix text here.",
  suffix: "Suffix text here.",
};

export const WithError = Template.bind({});
WithError.args = {
  ...WithDescription.args,
  errors: "Please make a selection.",
};
