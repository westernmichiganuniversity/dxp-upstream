import DrupalAttribute from "drupal-attribute";
import ContainerTwig from "./container.twig";
import { Button } from "../../../01-atoms/button/button.stories";
import {
  Element,
  OptionCheckbox,
  OptionRadio,
} from "../element/element.stories";

export default {
  title: "Molecules/Form/Container",
  argTypes: {
    hasParent: { control: { type: "boolean" } },
    type: { table: { disable: true } },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const children = args.children.join("");
  return ContainerTwig({ ...args, attributes, children });
};

export const Container = Template.bind({});
Container.args = {
  hasParent: false,
  type: "container",
  children: [Element({ ...Element.args }), Element({ ...Element.args })],
};

export const Checkboxes = Template.bind({});
Checkboxes.args = {
  ...Container.args,
  hasParent: true,
  children: [
    OptionCheckbox({ ...OptionCheckbox.args }),
    OptionCheckbox({
      ...OptionCheckbox.args,
      label: "Friend requests",
      description: "Get notified when someone send you a friend request.",
    }),
  ],
};

export const Radios = Template.bind({});
Radios.args = {
  ...Container.args,
  hasParent: true,
  children: [
    OptionRadio({ ...OptionRadio.args }),
    OptionRadio({
      ...OptionRadio.args,
      label: "Same as email",
    }),
    OptionRadio({
      ...OptionRadio.args,
      label: "No push notifications",
    }),
  ],
};

export const Actions = Template.bind({});
Actions.args = {
  ...Container.args,
  type: "actions",
  children: [Button({ ...Button.args, label: "Save", variant: "primary" })],
};
