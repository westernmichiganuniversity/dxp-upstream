import DrupalAttribute from "drupal-attribute";
import ElementTwig from "./element.twig";
import { Checkbox } from "../../../01-atoms/checkbox/checkbox.stories";
import { ColorPicker } from "../../../01-atoms/colorpicker/colorpicker.stories";
import { FileUpload } from "../../../01-atoms/fileupload/fileupload.stories";
import { Label } from "../../../01-atoms/label/label.stories";
import { Radio } from "../../../01-atoms/radio/radio.stories";
import { Range } from "../../../01-atoms/range/range.stories";
import { Search } from "../../search/search.stories";
import { Select } from "../../../01-atoms/select/select.stories";
import { TextArea } from "../../../01-atoms/textarea/textarea.stories";
import { TextField } from "../../../01-atoms/textfield/textfield.stories";

export default {
  title: "Molecules/Form/Element",
  decorators: [
    (story) => `<div class="tw-grid tw-grid-cols-6 tw-gap-6">${story()}</div>`,
  ],
  argTypes: {
    type: { table: { disable: true } },
    label: { control: { type: "text" } },
    labelDisplay: {
      control: { type: "radio" },
      options: ["before", "after", "invisible"],
    },
    description: { control: { type: "text" } },
    descriptionDisplay: {
      control: { type: "radio" },
      options: ["before", "after", "invisible"],
    },
    required: { control: { type: "boolean" } },
    prefix: { control: { type: "text" } },
    suffix: { control: { type: "text" } },
    errors: { control: { type: "text" } },
    containerClass: { control: { type: "text" } },
  },
};

const Template = ({
  autocomplete,
  containerClass,
  description,
  descriptionDisplay,
  label,
  labelDisplay,
  placeholder,
  required,
  rows,
  type,
  ...args
}) => {
  const attributes = new DrupalAttribute();
  if (containerClass) {
    const classes = containerClass.split(" ");
    attributes.addClass(classes);
  }

  const renderedLabel = Label({
    ...Label.args,
    for: "edit-story",
    label,
    variant: labelDisplay,
    isRequired: required,
  });

  const descriptionObject = {
    content: description,
    attributes: new DrupalAttribute(),
  };

  let children;
  switch (type) {
    case "checkbox":
      children = Checkbox({
        ...Checkbox.args,
        id: "edit-story",
        hasErrors: !!args.errors,
      });
      break;

    case "color":
      children = ColorPicker({
        ...ColorPicker.args,
        id: "edit-story",
        hasErrors: !!args.errors,
      });
      break;

    case "file":
      children = FileUpload({
        ...FileUpload.args,
        id: "edit-story",
        hasErrors: !!args.errors,
      });
      break;

    case "radio":
      children = Radio({
        ...Radio.args,
        id: "edit-story",
        hasErrors: !!args.errors,
      });
      break;

    case "range":
      children = Range({
        ...Range.args,
        id: "edit-story",
        hasErrors: !!args.errors,
      });
      break;

    case "search":
      children = Search({
        ...Search.args,
        id: "story",
        name: "story",
        isDisabled: false,
        isReadOnly: false,
        hasErrors: !!args.errors,
        size: "base",
        colorMode: "auto",
      });
      break;

    case "select":
      children = Select({
        ...Select.args,
        id: "edit-story",
        hasErrors: !!args.errors,
      });
      break;

    case "textarea":
      children = TextArea({
        ...TextArea.args,
        id: "edit-story",
        required,
        autocomplete,
        placeholder,
        rows,
        hasErrors: !!args.errors,
      });
      break;

    case "text":
    default:
      children = TextField({
        ...TextField.args,
        id: "edit-story",
        type,
        autocomplete,
        placeholder,
        hasPrefix: !!args.prefix,
        hasSuffix: !!args.suffix,
        hasErrors: !!args.errors,
      });
      break;
  }

  return ElementTwig({
    attributes,
    children,
    label: renderedLabel,
    labelDisplay,
    description: descriptionObject,
    descriptionDisplay,
    ...args,
  });
};

export const Element = Template.bind({});
Element.args = {
  type: "text",
  label: "Label",
  labelDisplay: "before",
  description: "",
  descriptionDisplay: "after",
  required: false,
  prefix: "",
  suffix: "",
  errors: "",
  containerClass: "tw-col-span-6",
};

export const WithDescription = Template.bind({});
WithDescription.args = {
  ...Element.args,
  containerClass: "tw-col-span-6 sm:tw-col-span-3",
  label: "First name",
  description: "Your preferred given name",
  autocomplete: "given-name",
};

export const InvisibleLabel = Template.bind({});
InvisibleLabel.args = {
  ...Element.args,
  label: "Email address",
  labelDisplay: "invisible",
  placeholder: "Email address",
  autocomplete: "email",
};

export const PrefixAndSuffix = Template.bind({});
PrefixAndSuffix.args = {
  ...Element.args,
  label: "Payment amount",
  type: "number",
  prefix: "$",
  suffix: ".00",
  placeholder: "0",
  containerClass: "tw-col-span-3 sm:tw-col-span-2",
};

export const OptionCheckbox = Template.bind({});
OptionCheckbox.args = {
  ...Element.args,
  type: "checkbox",
  label: "Comments",
  labelDisplay: "after",
  description: "Get notified when someone posts a comment.",
};

export const OptionRadio = Template.bind({});
OptionRadio.args = {
  ...Element.args,
  type: "radio",
  label: "Everything",
  labelDisplay: "after",
};

export const OptionSelect = Template.bind({});
OptionSelect.args = {
  ...Element.args,
  type: "select",
  label: "Country",
};

export const RangeExample = Template.bind({});
RangeExample.args = {
  ...Element.args,
  type: "range",
  label: "Volume",
};

export const TextAreaExample = Template.bind({});
TextAreaExample.args = {
  ...Element.args,
  type: "textarea",
  label: "About",
  rows: 3,
  description: "Brief description for your profile.",
};

export const WithError = Template.bind({});
WithError.args = {
  ...Element.args,
  required: true,
  errors: "Please enter a value for all required fields.",
};

export const ColorPickerExample = Template.bind({});
ColorPickerExample.args = {
  ...Element.args,
  type: "color",
  label: "Background color",
  labelDisplay: "after",
};

export const FileUploadExample = Template.bind({});
FileUploadExample.args = {
  ...Element.args,
  type: "file",
  label: "Attachment",
};

export const SearchExample = Template.bind({});
SearchExample.args = {
  ...Element.args,
  type: "search",
  label: "Search",
  labelDisplay: "invisible",
  containerClass: "tw-col-span-6 sm:tw-col-span-3",
};
