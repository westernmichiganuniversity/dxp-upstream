import DrupalAttribute from "drupal-attribute";
import BlockTwig from "./block.twig";

export default {
  title: "Molecules/Block",
  argTypes: {
    label: { control: "text" },
    level: {
      control: { type: "select" },
      options: [1, 2, 3, 4, 5, 6],
    },
    content: { control: "text" },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const labelAttributes = new DrupalAttribute();
  return BlockTwig({ attributes, labelAttributes, ...args });
};

export const Block = Template.bind({});
Block.args = {
  label: "A basic block",
  content: "Lorem ipsum sit amet",
};
