import DrupalAttribute from "drupal-attribute";
import SearchTwig from "./search.twig";

export default {
  title: "Molecules/Search",
  argTypes: {
    id: { table: { disable: true } },
    name: { table: { disable: true } },
    isDisabled: { control: { type: "boolean" } },
    isReadOnly: { control: { type: "boolean" } },
    hasErrors: { control: { type: "boolean" } },
    size: {
      control: { type: "select" },
      options: ["sm", "base", "lg", "xl"],
    },
    colorMode: {
      control: { type: "select" },
      options: ["auto", "light", "dark"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const textAttributes = new DrupalAttribute();
  const iconAttributes = new DrupalAttribute();
  const buttonAttributes = new DrupalAttribute();
  const knownAttributes = ["id", "name"];
  knownAttributes.forEach((attr) => {
    if (args[attr]) {
      textAttributes.setAttribute(attr, `${args[attr]}-text`);
      buttonAttributes.setAttribute(attr, `${args[attr]}-button`);
    }
  });
  return SearchTwig({
    attributes,
    textAttributes,
    iconAttributes,
    buttonAttributes,
    ...args,
  });
};

export const Search = Template.bind({});
Search.args = {
  id: "story",
  name: "story",
  isDisabled: false,
  isReadOnly: false,
  hasErrors: false,
  size: "base",
  colorMode: "auto",
};
