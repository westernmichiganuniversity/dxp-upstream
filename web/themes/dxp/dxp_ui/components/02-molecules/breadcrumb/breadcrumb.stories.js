import DrupalAttribute from "drupal-attribute";
import BreadcrumbTwig from "./breadcrumb.twig";

export default {
  title: "Molecules/Breadcrumb",
  argTypes: {
    breadcrumb: { control: { type: "object" } },
  },
};

const Template = ({ ...args }) => {
  const breadcrumb = [];
  args.breadcrumb.forEach((item) => {
    breadcrumb.push({
      attributes: new DrupalAttribute(),
      href: item.href,
      label: item.label,
    });
  });
  return BreadcrumbTwig({ ...args, breadcrumb });
};

export const Breadcrumb = Template.bind({});
Breadcrumb.args = {
  breadcrumb: [
    {
      label: "Home",
      href: "https://wmich.edu",
    },
    {
      label: "Haworth",
      href: "https://haworth.wmich.edu",
    },
    {
      label: "Academics",
      href: "https://haworth.wmich.edu/academics",
    },
    {
      label: "Master of Business Administration",
      href: "https://haworth.wmich.edu/academics/mba",
    },
  ],
};
