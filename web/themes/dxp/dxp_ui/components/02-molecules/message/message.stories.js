import DrupalAttribute from "drupal-attribute";
import MessageTwig from "./message.twig";

export default {
  title: "Molecules/Message",
  argTypes: {
    label: { control: "text" },
    level: {
      control: { type: "select" },
      options: [1, 2, 3, 4, 5, 6],
    },
    content: { control: "text" },
    status: {
      control: { type: "select" },
      options: ["info", "notice", "positive", "negative"],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const labelAttributes = new DrupalAttribute();
  return MessageTwig({ attributes, labelAttributes, ...args });
};

export const InfoMessage = Template.bind({});
InfoMessage.args = {
  content:
    "<p>The following HTML tags are allowed: <code>&lt;p&gt; &lt;ul&gt; &lt;ol&gt; &lt;li&gt; &lt;strong&gt; &lt;a&gt;</code></p>",
  status: "info",
};

export const NoticeMessage = Template.bind({});
NoticeMessage.args = {
  label: "Weather closure",
  content:
    "Due to inclement weather, WMU is closed and all classes and events are cancelled <strong>Monday, Dec. 13, 2021</strong>.",
  status: "notice",
};

export const PositiveMessage = Template.bind({});
PositiveMessage.args = {
  content: "Your form submission has been received.",
  status: "positive",
};

export const NegativeMessage = Template.bind({});
NegativeMessage.args = {
  label: "An error occurred while saving your changes.",
  content: "Please contact your system administrator for assistance.",
  status: "negative",
};
