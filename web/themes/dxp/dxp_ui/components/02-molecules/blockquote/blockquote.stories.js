import DrupalAttribute from "drupal-attribute";
import BlockquoteTwig from "./blockquote.twig";

export default {
  title: "Molecules/Blockquote",
  argTypes: {
    text: { control: { type: "text" } },
    attribution: { control: { type: "text" } },
    citation: { control: { type: "text" } },
    halign: {
      control: { type: "select" },
      options: ["left", "center", "right"],
    },
    color: {
      control: { type: "select" },
      options: [
        "blue",
        "brown",
        "cinnabar",
        "cool_gray",
        "gold",
        "gray",
        "green",
        "lilac",
        "red",
        "teal",
        "warm_gray",
      ],
    },
  },
};

const Template = ({ ...args }) => {
  const attributes = new DrupalAttribute();
  const textAttributes = new DrupalAttribute();
  return BlockquoteTwig({ attributes, textAttributes, ...args });
};

export const Blockquote = Template.bind({});
Blockquote.args = {
  text: "<p>I've had the opportunity and been challenged to further develop introspection, interpersonal relationships and be a lifelong learner as I depart ... I'm happy I chose Western because of the community we have here; we strive for excellence for every student in every type of way.</p>",
  attribution: "Kendall Owens '19, nursing and dance (from Plymouth, Michigan)",
  halign: "left",
  color: "cinnabar",
};
