/**
 * Displays a character limit counter for the given element.
 *
 * @param {HTMLElement} element
 *   An input or textarea element.
 * @param {HTMLElement} countContainer
 *   The wrapper element for the character counter.
 */
function characterLimit(element, countContainer) {
  countContainer.classList.add("tw-float-right", "tw-leading-[inherit]");
  const counter = document.createElement("span");
  counter.classList.add("tw-text-sm");
  countContainer.appendChild(counter);
  element.addEventListener("input", (e) => {
    const { target } = e;
    const maxLength = target.getAttribute("maxlength");
    const currentLength = target.value.length;
    counter.innerHTML = `${currentLength}/${maxLength}`;
  });
}

export default characterLimit;
